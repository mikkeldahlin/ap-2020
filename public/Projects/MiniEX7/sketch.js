let squares = [];
let myAnts = [];
let tempAnt;
let cols;
let rows;
let moveset = ['1;1', '1;-1', '-1;1', '-1;-1', '0;1', '1;0', '-1;0', '0;-1'];
let space = 40; //size of squares - this defines all the math in the program

function make2Darray(cols, rows) {
  let arr = new Array(cols);
  for (i = 0; i < arr.length; i++) {
    arr[i] = new Array(rows);
  }
  return arr;
}

function setup() {
  createCanvas(windowWidth, windowHeight);

  frameRate(5);

  cols = floor(width / space); //cannot calculate these in global call, as the width and height is only set after create canvas
  rows = floor(height / space); //

  squares = make2Darray(cols, rows);

  for (var i = 0; i < cols; i++) {
    for (var j = 0; j < rows; j++) {
      squares[i][j] = 0;
    }
  }
}

function draw() {
  for (i = 0; i < myAnts.length; i++) {
    myAnts[i].move();
  }

  for (i = 0; i < cols; i++) {
    for (j = 0; j < rows; j++) {
      let x = i * space;
      let y = j * space;
      fill(squares[i][j]);
      stroke(0);
      rect(x, y, space, space);
    }
  }
}

function mouseClicked() {
  tempAnt = new Ants(floor(mouseX / space), floor(mouseY / space));
  myAnts.push(tempAnt);
}
