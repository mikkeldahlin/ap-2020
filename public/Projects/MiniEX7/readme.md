https://mikkeldahlin.gitlab.io/ap-2020/Projects/MiniEX7/

click and move the mouse to start the chaos

What are the rules in your generative program and describe how your program performs over time and how are the running of the rules contingently enabled emergent behaviors? <br>
<br>
I wanted to create chaos from randomness. Starting with seemingly nothing, a blank canvas.  With a mousepress you create an ant. That ant will be awarded a random shade of grey and color the tile under it with that shade. The ant will move in a random direction each frame and color the new tile with its shade, expanding its territory. With each move, an ant has a random change of dying. Splicing itself from the array of ants. If an ant moves to a tile already in its own territory, it can reproduce, creating another ant with the same shade, and there for increasing its change of dominion. If there is at least one ant alive, there will occasionally spawn a new an on mouse pos. There can therefor only be a “winner” if all ants are dead. <br>

1.	Start with nothing
2.	Ants spawn on mousepress
3.	Ant will color tiles with its own shade
4.	Ant will move in a random direction each frame 
5.	Ants will reproduce if standing in its own shade
6.	Ant have a random change of dying
7.	If another ant is alive, a new will spawn on mousepos

What's the role of rules and processes in your work? <br>
<br>
I guess I wanted to simulate a little war. There are teams in the sense of the shade an ant is attributed with. There are death and new recruitments. Potentially (although statistically improbable) there can be alliances between ant that have been active for some time and a new ant spawned with the same shade.<br>
<br>
Draw upon the assigned reading(s), how does this mini-exericse help you to understand auto generator (e.g control, autonomy, instructions/rules)? Do you have any further thoughts about the theme of this chapter? <br>
<br>
I guess the biggest lesson I learned about auto generation and randomness; is how hard it is to control (which I guess is exactly the point). I wanted to make the program in a way that there would most likely be a winner within a few minutes. But these ants are just impossible to control. Either the first ant dies in 3 frames and no others are spawned, or a 1000 ants are spawned and there seems to be no end to the war.
This exercise was also a great way to learning about the programming of stuff that seems alive. The rules that control your creations makes it seem like they have a choice, in the case of my program it almost reminds me of natural selection. A ant can either “choose” to expand its territory or reproduce, adding to its ranks. My program needs some adjusting to truly showcase this though. Maybe adding some rules about an ant will die if it ends its turn in another shade than its own – this way newer ants can seemingly kill older ants. Give ants a longer lifetime and let less ants spawn. 
