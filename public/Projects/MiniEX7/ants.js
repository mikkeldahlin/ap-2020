class Ants{
  constructor(x, y, c) {
    this.x = x;
    this.y = y;
    this.c = random(255);
  }

  move() {
    if (squares[this.x][this.y] == this.c) { //reproduceing function
      tempAnt = new Ants(floor(mouseX / space), floor(mouseY / space));
      myAnts.push(tempAnt);
    }

    squares[this.x][this.y] = this.c;

    let move = random(moveset); //picks a random moveset from the array
    let nextmove = split(move, ';'); //splits the moveset in x and y
    let tempX = parseInt(nextmove[0]); //changes the content of the variable from a string to an integer
    let tempY = parseInt(nextmove[1]);

    this.x = this.x + tempX;
    this.y = this.y + tempY;

    if (this.x > cols-1) { //minus one to take care of excess non exsistant squares in the squars array
      this.x = 0;
    }
    if (this.x < 0) {
      this.x = cols-1;
    }
    if (this.y > rows) {
      this.y = 0;
    }
    if (this.y < 0) {
      this.y = rows;
    }

    let dead = int(random(50)); //randomly kills ants
    if (dead == 1) {
      myAnts.splice(this, 1);
    }
  }
}
