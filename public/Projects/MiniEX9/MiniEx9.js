var thoughts = [];

var api = 'https://api.whatdoestrumpthink.com/api/';
var personalizedQuate = 'v1/quotes/personalized?q=';

var button;
var input;

function setup() {
  var cnv = createCanvas(windowWidth, windowHeight);
  cnv.style('display', 'block');

  input = createInput();
  button = createButton('Search');

  input.position(width/2 - input.width/2 - button.width/2, 50);
  button.position(input.x + input.width, 50);

  button.mousePressed(trumpThoughts);

  textAlign(CENTER);
}

function trumpThoughts() {
  search = input.value();
  var url = api + personalizedQuate + search;
  $.getJSON(url, gotData); //jquery lib is used here, as p5's own loadJSON function returned an error, Thanks to Winnie for the input here
}

function gotData(data) {
  temp = data.message;
  thoughts.push(temp);
}

function draw() {
  background(255);
  text("What does Trump Think about:", width/2, 30)
  for (i = 0; i < thoughts.length; i++) {
    text(thoughts[i], width/2, 100+i*25);
  }
}
