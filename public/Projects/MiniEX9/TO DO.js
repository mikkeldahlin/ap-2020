to do
// TODO:

search bar xoxo

get data button xoxo

list of results

//TRUMP
Get api to work properly
positioning of elements
  input and output styling

output string
    text styling
      how do we want to display it?
        possibly more meaning?
Other elements?
  movement
  higlighting keywords
  appearing text
  other animated/aesthetic things

  /*
   Fetching other domain images via Google image search API has CORS problem (Cross-Origin Resource Sharing):
  - proper fixing should be done at the web server side but for testing purpose we can install browser add-ons to by-pass the errors, keyword search: CORS, or add a proxy in front of the image URL
    - firefox ver72.02: Access Control-Allow-Origin - Unblock: https://addons.mozilla.org/en-US/firefox/addon/cors-unblock/
    - Chrome ver79: CORS Unblock https://chrome.google.com/webstore/detail/cors-unblock/lfhmikememgdcahcdlaciloancbhjino
    - we can also use CORS proxy: see line 39
  - credit: Image Lines in Processing by Anna the Crow https://www.openprocessing.org/sketch/120964
  - full url here: https://www.googleapis.com/customsearch/v1?key=???&cx=????&imgSize=small&q=warhol+flowers
  */
  let url = "https://www.googleapis.com/customsearch/v1?";
  let apikey = "AIzaSyCzCsHryNrirPWVanCv-pq7CWahRjY0usY";  //register API key here: https://developers.google.com/custom-search/json-api/v1/overview
  let engineID = "007442548860781402246:fdkcyve9lgt"; //https://cse.google.com/all  | create search engine, then get the searchengine ID - make sure image is on
  let query = "life+death";  //search keywords
  let searchType = "image";
  let imgSize ="medium"; //check here: https://developers.google.com/custom-search/json-api/v1/reference/cse/list#parameters
  let request; //full API

  let getImg;
  let loc;
  let img_x, img_y;
  let imgCORSproxy = "https://cors-anywhere.herokuapp.com/"; //check top comment
  let frameBorder = 60;  //each side
  let imgLoaded = false;

  function setup() {
  	createCanvas(windowWidth,windowHeight);
  	background(255);
  	frameRate(10);
  	fetchImage();
  }

  function fetchImage() {
  	request = url + "key=" + apikey + "&cx=" + engineID + "&imgSize=" + imgSize + "&q=" + query + "&searchType=" + searchType;
  	console.log(request);
  	loadJSON(request, gotData); //this is the key syntax and line of code to make a query request and get a query response
  }

  function gotData(data) {
  	getImg = imgCORSproxy + data.items[0].link;  //add the CORS proxy to bypass browser's security
  	console.log(getImg);
  }

  function draw() {
  	if (getImg){ //takes time to retrieve the API data
        loadImage(getImg, img=> { //callback function
          //frame + image
          push();
          translate(width/2-img.width/2-frameBorder, height/2-img.height/2-frameBorder);
          if (!imgLoaded) {
            noStroke();
            fill(239);
            rect(0,0,img.width+frameBorder*2, img.height+frameBorder*2);
            //image + status
  	        image(img,0+frameBorder,0+frameBorder);
            imgLoaded = true;
          } else {
          //draw lines
            img.loadPixels();
            img_x = floor(random(0,img.width));
  	        img_y = floor(random(0,img.height));
            loc = (img_x+img_y * img.width)*4; // The formular to locate the no: x+y*width, indicating which pixel of the image in a grid (and each pixel array holds red, green, blue and alpha values - 4) can see more here: https://www.youtube.com/watch?v=nMUMZ5YRxHI
            stroke(color(img.pixels[loc],img.pixels[loc + 1], img.pixels[loc+2]));  //rgb values
            line(frameBorder+img_x,frameBorder+img_y,frameBorder+img_x,frameBorder+img.height);
          }
          pop();
      });
   }
  }
