<h1>Global Stories</h1>
Mikkel Dahlin & Andreas Frederiksen<br>
<br>
![ScreenShot](https://i.postimg.cc/q7KQR9gq/ss.png)
<br>
Dont reload, some issues might happen if you computer is not powerfull enough that causes the video background to not load <br>
<br>
https://mikkeldahlin.gitlab.io/ap-2020/Projects/MiniEX8/<br>
<br>
if you are having issues with the program, a video of it have been made and posted here: https://www.youtube.com/watch?v=g5kxalWvFAc&feature=youtu.be<br>
<br>
<h2>Provide</h2>
Our program is stories of the different perspectives people experience regarding the coronavirus pandemic. Stories from all walks of life, to show the complexity and impact something of this character has on a global yet very human scale. All experience the same event, yet their interpretation and involvement takes on different shapes and emotions. the world is encompassed by an event none have experienced the likes of before, changing the daily life and functions of what we call normal living, a sudden upturning and unrooting of deep set emotions, calling forth the reptilian survival instinct in some, and empathic response in others. Sometimes broadening of perspective helps us understand, see things in a new light and especially in struggling times like these, come together in close, yet physically distant unity. <br>
<br>
<br>
<h2>Describe</h2>
A short program that consists of a background, a text overlay, a song and a json file. <br>
<br>
The background is a video made up of a mix of images taken around the world's busy cities web cameras during their respective lockdowns and quarantined states, showing a different version of the buzzling cities we’ve become accustomed to. The program uses this video as an image that fills out the entire canvas and functions as the background. <br>
<br>
The text overlay is controlled using a function that fades in the text, keeps it full opacity for a little while, and then fades it out again. When the text reaches 0 opacity, it will change the string and fade the new string back in to repeat the process once again. <br>
The text has been styled with an easy to read font, and with a hard stroke line to ensure that it would be readable even when the background changes from dark to light.<br>
<br>
To add an underlying tone to our program, we’ve accompanied the slowly fading text and changing background with a song that is both hopeful, yet melancholic. giving an opportunity for the user of the program to transport themselves into different emotional roles, much to their own preference and interpretation of the message. leaving the serious emotions of the program up to the user, rather than forcing a response. <br>
<br>
The json file is a complement of the data we use as strings in the text overlay. The data have been gathered using a mixture of online research (articles and interviews) as well as questioning people we know and whose perspective we thought might be interesting to include.<br>
<br>
<br>
<h2>Analyze and articulate</h2>
During our work on the program visual elements and raw code, we were very aware of the elements we decided to include or not include, we wanted to not just make a visually pleasing program that makes you think of the pandemic from a new perspective. But also something more meaningful hidden in the code. <br>
“As software becomes ubiquitous it becomes ever more connected to external processes, and programs no longer encode pure logic but human social behavior too. Such approaches recognize that systems are embedded in larger language systems where meanings are produced through social practices.“ Speaking Code - Geoff Cox (p. 26-27)<br>
As Geoff Cox explains, we have tried to link our program to our current situation by naming variables, functions and even the JSON file to tell key points of the current situation, almost like a story in itself. And thereby interlinking our code with what we are trying to display with the program itself. An example of this includes lines such as; “stories = humanitys.data;” referring to the main premise of our program, to tell the stories of people, and in extension humanity. another example would be “outside.play();” and “outside.loop();”  rather than using “song.play” and “song.loop” a more theatrical and meaningful line is created, exposing what some of our respondents even said, they missed their freedom to move, being outside, social life. and to extend, the song name of the variable “outside” is “echoes” to represent the memories or distant echoes of their “old life” before the pandemic. There are a multitude of these, echoing the same sentiment, “isolation.loop” referencing the repetitive life of isolation and “isolation.hide” to showcase the feeling of hiding in one's own home, from an invisible “enemy”. <br>
Different lines of our code also function as real sentences, taking the code into a more poetic direction, utilizing the practice of “code act” as mentioned by Hayles. the way we speak and write, and in our program read and see, influence the way we experience. the code in our program exceeds these boundaries of reading and writing. resulting in a readable code, a visual representation of the data and the layers of abstraction that are in between the lines of code, and the executing parameters that affect the visual side of things.<br>
<br>
The program tries to tell the story of the corona pandemic from another perspective, to highlight the individual and express that we are all in the situation together but differently and separately. Much like the way we have had to adapt to this new world, standing together but separately. The program does not try to display the surreal imagery in the background as sad, but - with help from the song - as something seren, unbiased and introspective.
<br>
<br>
<h3>Articles, videos and literature used for research and citation</h3>
Cox, Geoff, and Alex McLean. Speaking Code. Cambridge, Mass.: MIT Press, 2013. 17-38.<br>
https://www.theguardian.com/world/2020/mar/11/coronavirus-wuhan-doctor-ai-fen-speaks-out-against-authorities<br>
https://www.youtube.com/watch?v=vjJbLJqcVSY<br>
https://abc7chicago.com/6033993/<br>
https://www.youtube.com/watch?v=G9oqvJ3iXGI<br>
https://news.trust.org/item/20200323180546-mnuof/
