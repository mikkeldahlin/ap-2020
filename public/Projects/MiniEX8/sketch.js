var humanitys;
var stories;
var why;
var isolation;
var communication;
var life = 1;
var death = 1;
var outside;

function preload(){
  humanitys = loadJSON('earth.json');
  isolation = createVideo("quarantine.mp4");
    isolation.loop();
    isolation.hide();
  communication = loadFont('seperated.TTF');
  outside = loadSound("echoes.mp3");
}

function setup(){
  var cnv = createCanvas(windowWidth, windowHeight);
  cnv.style('display', 'block');
  stories = humanitys.data;
  why = floor(random(0, stories.length));
  outside.play();
    outside.loop();
}

function draw(){
  image(isolation,0,0)
  Tales();
  Fade();
}

function Tales(){
  //styling of text
  textSize(width/27);
  fill(255, life);
  stroke(0, life);
  strokeWeight(4)
  textAlign(CENTER);
  textFont(communication);
  translate(width/2, height/2);
  //data output and compile
  text("I am " + stories[why].iAm + ".", 0, -100);
  text("Its been " + stories[why].itsBeen + ",", 0, 0);
  text("it feels " + stories[why].itFeels + ",", 0, 100);
  text("I miss " + stories[why].iMiss + ".", 0, 200);
}

function Fade(){
  life += death;

  if (life == 0){ // changes between data points when the previous have faded out and changes to fadeing in
    why = floor(random(0, stories.length));
    death *= -1;
  }

  if ( life == 400){ // functions as a timer
    death *= -1;
  }
}
