let items = [];
let lastClicked = 0;
let gridEnabled = false;
let gridDist = 25;
let gridAlpha = 90;
let hidden = false;
let action = move;

function setup() {
  const canvas = createCanvas(windowWidth, windowHeight);
  canvas.style('display', 'block');
  canvas.drop(gotFile);
  imageMode(CENTER);
  hideButton = createButton('HIDE');
  hideButton.position(0, 0);
  hideButton.size(70, 20);
  hideButton.mousePressed(hideInterface);
  gridButton = createButton('GRID');
  gridButton.position(0, 20);
  gridButton.size(70, 20);
  gridButton.mousePressed(grid);
  slider = createSlider(10, 150, 25, 0.1);
  slider.position(70, 0);
  slider.style('width', '800px');
  alphaSlider = createSlider(10, 255, 90, 1);
  alphaSlider.position(70, 20);
  alphaSlider.style('width', '800px');
  moveButton = createButton('MOVE');
  moveButton.position(0, 40);
  moveButton.size(70, 20);
  moveButton.mousePressed(move);
  rotateButton = createButton('ROTATE');
  rotateButton.position(0, 60);
  rotateButton.size(70, 20);
  rotateButton.mousePressed(rotateImg);
  flipButton = createButton('FLIP');
  flipButton.position(0, 80);
  flipButton.size(70, 20);
  flipButton.mousePressed(flipImg);
}

function draw() {
  background(0);
  for (i = 0; i < items.length; i++) {
    items[i].show();
    items[i].update();
  }
  if (gridEnabled) {
    gridDist = slider.value();
    gridAlpha = alphaSlider.value();
    stroke(255, gridAlpha);
    for (i = 0; i < width/gridDist; i++) {
      line(i*gridDist, 0, i*gridDist, height);
    }
    for (i = 0; i < height/gridDist; i++) {
      line(0, i*gridDist, width, i*gridDist);
    }
  }
}

function gotFile(file) {
  if (file.type === 'image') {
    img = createImg(file.data, "Error").hide();
    tempItem = new Obj(img, img.width, img.height);
    items.push(tempItem);
  }
}

function mousePressed() {
  if (items.length > 0) {
    let closest = 10000;
    for (i = 0; i < items.length; i++) {
      var d = dist(mouseX, mouseY, items[i].x, items[i].y);
      if (d < closest) {
        closest = d;
        lastClicked = i;
      }
    }
    items[lastClicked].drag();
  }
}

function mouseReleased() {
  if (items.length != 0) {
    items[lastClicked].drop();
  }
}

function mouseWheel(event) {
  if (items.length != 0) {
    items[lastClicked].scale -= event.delta/10000;
  }
}

function keyPressed() {
  if (keyCode === 46) {
    items.splice(lastClicked, 1);
  }

  if (keyCode === 32) {
    if (hidden) {
      hidden = false;
      gridButton.show();
      slider.show();
      hideButton.show();
      alphaSlider.show();
      moveButton.show();
      rotateButton.show();
      flipButton.show();
    }
    else {
      hidden = true;
      gridButton.hide();
      slider.hide();
      hideButton.hide();
      alphaSlider.hide();
      moveButton.hide();
      rotateButton.hide();
      flipButton.hide();
    }
  }
}

function hideInterface() {
  hidden = true;
  gridButton.hide();
  slider.hide();
  hideButton.hide();
  alphaSlider.hide();
  moveButton.hide();
  rotateButton.hide();
  flipButton.hide();
}

function grid() {
  gridEnabled = !gridEnabled;
}

function move() {
  action = move;
}

function rotateImg() {
  action = rotate;
}

function flipImg() {
  if (items.length != 0) {
    items[lastClicked].flip();
  }
}
