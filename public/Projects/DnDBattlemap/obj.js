class Obj {
  constructor(img, w, h) {
    this.img = img;
    this.width = w;
    this.height = h;
    this.x = width/2;
    this.y = height/2;
    this.dragging = false;
    this.scale = 1;
    this.newAngle = 0;
    this.oldAngle = 0;
    this.rotate = 0;
    this.firstClick = false;
    this.lastDir = 0;
  }

  show() {
    push();
      translate(this.x, this.y);
      rotate(this.rotate);
      image(this.img, 0, 0, this.width * this.scale, this.height * this.scale);
    pop();
  }

  update() {
    if (this.dragging) {
      if (action === move) {
        this.x = mouseX + this.offsetX;
        this.y = mouseY + this.offsetY;
      }
      else if (action === rotate && mouseY > 40) {
        this.oldAngle = this.newAngle;
        push();
          let v0 = createVector(0, 0);
          let v1 = createVector(100, 0);
          let v2 = createVector(mouseX - this.x, mouseY - this.y);
        pop();
        this.newAngle = v1.angleBetween(v2);

        this.rotate += this.newAngle - this.oldAngle;
      }
    }
  }

  drag() {
    if (mouseY > 40) {
      if (this.firstClick) {
        this.firstClick = false;
        push();
          let v0 = createVector(0, 0);
          let v1 = createVector(100, 0);
          let v2 = createVector(mouseX - this.x, mouseY - this.y);
        pop();
        this.newAngle = v1.angleBetween(v2);
      }
      this.dragging = true;
      this.offsetX = this.x - mouseX;
      this.offsetY = this.y - mouseY;
    }
  }

  drop() {
    this.dragging = false;
    this.firstClick = true;
  }

  flip() {
    this.lastDir++;
    if (this.lastDir === 4) {
      this.lastDir = 0;
    }
    this.rotate = 1.5707964*this.lastDir;
  }
}
