ANT LIFE
click to spawn an ant

[RunMe](https://mikkeldahlin.gitlab.io/ap-2020/Projects/MiniEX7.1/)

![ant.gif](ant.gif)

What are the rules in your generative program and describe how your program performs over time and how are the running of the rules contingently enabled emergent behaviors? 

I wanted to create chaos from randomness in the sense that you have no control over what happens. Starting with seemingly nothing, a blank canvas.  With a mousepress you create an ant. That ant will be awarded a random shade of grey and color the tile under it with that shade i the end of the turn (meaning when the draw function has run once). The ant will move in a random direction each turn and color the new tile with its shade, expanding its territory. With each move, an ant has a random change of dying, splicing itself from the array of ants. If an ant moves to a tile already in its own territory, it can reproduce, creating another ant with the same shade, and there for increasing its change of dominion. If there are two ants alive on the same tile, the oldest will die. The for loop that runs though the shading of tiles, does so by going though the array of ants, the lower numbers first. Meaning that a younger ant (higher number in the array), will color the tiles last. If an ant does not stand in its own shade by the start of its turn, it will die. The game only ends when one shade has monopoly on all the tiles. Even than, the game is only done visually. Behind the scene, within the code, the array of ants keeps growing with no end. The shade of these are all the same though, so no visual change will happen. Even if you where to click, the signle new ant would be overrun right away. 

1. Start with nothing
2. Ants spawn on mousepress
3. Ant will colour tiles with its own shade at the end of the turn
4. Ant will move in a random direction each frame
5. Ants have a chance of reproducing if standing in its own shade
6. Ant have a random change of dying
7. Ants will die when fighting with a younger ant about a tile


What's the role of rules and processes in your work? 

I guess I wanted to simulate a little war. There are teams in the sense of the shade an ant is attributed with. There are death and new recruitments. Potentially (although statistically improbable) there can be alliances between an ant that have been active for some time and a new ant spawned with the same shade.
This could also represent the early age man and the move from tribalisme towards a unified people. Or it could resemble the buissness strategy empolyed by companies like Google, Facebook and Amazon. Grow, consume and repeat. 

Draw upon the assigned reading(s), how does this mini-exericse help you to understand auto generator (e.g control, autonomy, instructions/rules)? Do you have any further thoughts about the theme of this chapter? 

The biggest lesson I learned about auto generation and randomness; is how hard it is to control (which I guess is exactly the point). I wanted to make the program in a way that there would most likely be a winner within a few minute. But these ants are just impossible to control. Either the first ant dies in 3 frames and no others are spawned, or 1000 ants are spawned and there seems to be no end to the war.
This exercise was also a great way to learning about the programming of stuff that seems alive. The rules that control your creations makes it seem like they have a choice, in the case of my program it almost reminds me of natural selection. A ant can either “choose” to expand its territory or reproduce, adding to its ranks. My program needs some adjusting to truly showcase this though. Maybe adding some rules about an ant will die if it ends its turn in another shade than its own – this way newer ants can seemingly kill older ants. Give ants a longer lifetime and let less ants spawn.

I have changed the ruleset of my miniEx7 to control the chaos a bit more. There is now a decent change of a winner among the ants. Alos I fixed a bug where, if an ant should reproduce, it would not spawn one in the same shade and it would be spawned at mouseX, mouseY. Reproduction have also been given a random factor. 

This have been heavily inspired by Langton's Ant and The game of life.
