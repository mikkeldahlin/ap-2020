class Ants{

  constructor(x, y, c) {
    this.x = x;
    this.y = y;
    this.c = c;
  }

  move() {
    let move = random(moveset); //picks a random moveset from the array
    let nextmove = split(move, ';'); //splits the moveset in x and y
    let tempX = parseInt(nextmove[0]); //changes the content of the variable from a string to an integer
    let tempY = parseInt(nextmove[1]);

    this.x = this.x + tempX;
    this.y = this.y + tempY;

    if (this.x > cols-1) { //minus one to take care of excess non exsistant squares in the squars array
      this.x = 0;
    }
    if (this.x < 0) {
      this.x = cols-1;
    }
    if (this.y > rows) {
      this.y = 0;
    }
    if (this.y < 0) {
      this.y = rows;
    }

    squares[this.x][this.y] = this.c;
  }

  reproduce() {
    if (squares[this.x][this.y] == this.c) { //randomly reproduces if in a tiles of same shade
      let reproduceing = int(random(10));
      if (reproduceing == 1) {
        tempAnt = new Ants(this.x, this.y, this.c);
        myAnts.push(tempAnt);
      }
    }
  }

  die() {
    let dead = int(random(15));
    if (dead == 1) { //randomly kills ants
      myAnts.splice(this, 1);
    }
    if (squares[this.x][this.y] != this.c) { //if a old ant finds itself in the same tiles as a younger ant from another shade at the end of its turn, it will die
      myAnts.splice(this, 1);
    }
  }
}
