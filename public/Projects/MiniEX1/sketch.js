function setup() {

  createCanvas(1000,800);
  background(250);

  r = 255;
  g = 255;
  b = 255;

}

function draw() {

//red
  stroke(1);
  fill(255,0,0);
  circle(90,20,30);

  if (mouseIsPressed) {
    let d = dist(mouseX, mouseY, 90, 20);
    if (d < 15) {
      r = 255;
      g = 0;
      b = 0;
    }
  }

//Orange
  stroke(1);
  fill(255,127,0);
  circle(125,30,30);

  if (mouseIsPressed) {
    let d = dist(mouseX, mouseY, 125, 30);
    if (d < 15) {
      r = 255;
      g = 127;
      b = 0;
    }
  }

//yellow
  stroke(1);
  fill(255,255,0);
  circle(150,55,30);

  if (mouseIsPressed) {
    let d = dist(mouseX, mouseY, 150, 55);
    if (d < 15) {
      r = 255;
      g = 255;
      b = 0;
    }
  }

//greenyellow
  stroke(1);
  fill(127,255,0);
  circle(160,90,30);

  if (mouseIsPressed) {
    let d = dist(mouseX, mouseY, 160, 90);
    if (d < 15) {
      r = 127;
      g = 255;
      b = 0;
    }
  }

//green
  stroke(1);
  fill(0,255,0);
  circle(150,125,30);

  if (mouseIsPressed) {
    let d = dist(mouseX, mouseY, 150, 125);
    if (d < 15) {
      r = 0;
      g = 255;
      b = 0;
    }
  }

//greencyan
  stroke(1);
  fill(0,255,127);
  circle(125,150,30);

  if (mouseIsPressed) {
    let d = dist(mouseX, mouseY, 125, 150);
    if (d < 15) {
      r = 0;
      g = 255;
      b = 127;
    }
  }

//cyan
  stroke(1);
  fill(0,255,255);
  circle(90,160,30);

  if (mouseIsPressed) {
    let d = dist(mouseX, mouseY, 90, 160);
    if (d < 15) {
      r = 0;
      g = 255;
      b = 255;
    }
  }

//bluecyan
  stroke(1);
  fill(0,127,255);
  circle(55,150,30);

  if (mouseIsPressed) {
    let d = dist(mouseX, mouseY, 55, 150);
    if (d < 15) {
      r = 0;
      g = 127;
      b = 255;
    }
  }

//blue
  stroke(1);
  fill(0,0,255);
  circle(30,125,30);

  if (mouseIsPressed) {
    let d = dist(mouseX, mouseY, 30, 125);
    if (d < 15) {
      r = 0;
      g = 0;
      b = 255;
    }
  }

//bluemagenta
  stroke(1);
  fill(127,0,255);
  circle(25,90,30);

  if (mouseIsPressed) {
    let d = dist(mouseX, mouseY, 25, 90);
    if (d < 15) {
      r = 127;
      g = 0;
      b = 255;
    }
  }

//magenta
  stroke(1);
  fill(255,0,255);
  circle(30,55,30);

  if (mouseIsPressed) {
    let d = dist(mouseX, mouseY, 30, 55);
    if (d < 15) {
      r = 255;
      g = 0;
      b = 255;
    }
  }

//redmagenta
  stroke(1);
  fill(255,0,127);
  circle(55,30,30);

  if (mouseIsPressed) {
    let d = dist(mouseX, mouseY, 55, 30);
    if (d < 15) {
      r = 255;
      g = 0;
      b = 127;
    }
  }

//white
  stroke(1);
  fill(255,255,255);
  circle(90,90,50);

  if (mouseIsPressed) {
    let d = dist(mouseX, mouseY, 90, 90);
    if (d < 25) {
      r = 255;
      g = 255;
      b = 255;
    }
  }

//drawing
  if (mouseIsPressed) {
    fill(r,g,b);
    noStroke();
    ellipse(mouseX, mouseY, 25);
    print(mouseX,mouseY);
  }
  print(mouseIsPressed);

}
