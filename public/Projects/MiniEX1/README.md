![ScreenShot](ss.png)

https://mikkeldahlin.gitlab.io/ap-2020/Projects/MiniEX1

Programming is a canvas, ready for whatever you want to put on it. I do not find programming hard, but i do think it hard to program the "right way" 
or the "smart" way. In this MiniEX i was able to make my simple paint clone, with a lot of if statements and a bit of math. But I cannot imagine that 
this is the smartest or easiest way to code something like this. I thought there must be a way to define an action when you click something like an ellipse. 
But i have been unable to find a way to make it work like that. Another issue I had was when i wanted to make a circle follow the mouse around, without
leaving a mark on the canvas. Kind of like a preview of where you would draw. But I realized that the only way I knew how to do that, was to write the 
background under der draw function, which would then mean that the user would be unable to paint, as the background would erase everything. No matter how 
much I googled, I was unable to find a fix for this, or the "smart" way to program it. I hope that by the end of this class I will be able to quickly google 
me to most coding fixes and use the references effectively.

I find that learning to program is much less like learning a new language and much more as learning to think logically. "If I write this, how does that 
translate to this other thing". Sure, a lot of time goes into looking up syntax and that was "learning the language". But much more time - at least for me -
goes into thinking about why the computer interbit a specific line of code, the way it does. Also, I tried to think about how I could optimize my program. 
Like the brush to change colour only when you click a circle, not just when you press the mouse over the arear of a circle. But for some reason I break 
the program when I try to make it so. For now, I am content with making whatever I set out to. But in the future, I would like to be able to make something 
the way I want to.


