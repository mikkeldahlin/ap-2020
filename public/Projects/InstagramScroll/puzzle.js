class PuzzlePiece {
  constructor(x, y, w, h, n, im, sourcex, sourcey, sourcew, sourceh) {
      this.x = x;
      this.y = y;
      this.originalX = x;
      this.originalY = y
      this.w = w;
      this.h = h;
      this.offsetX;
      this.offsetY;
      this.dragging = false;
      this.number = n;
      this.image = im;
      this.sourceX = sourcex;
      this.sourceY = sourcey;
      this.sourceW = sourcew;
      this.sourceH = sourceh;
  }

  update () {
    if (this.dragging) {
      this.y = this.offsetY + mouseY;
      this.x = this.offsetX + mouseX;
    }
    else {
      this.y = this.originalY - scrollOffset;
      this.x = this.originalX;
    }
  }

  show () {
    image(this.image, this.x, this.y, this.w, this.h, this.sourceX, this.sourceY, this.sourceW, this.sourceH)
  }
}
