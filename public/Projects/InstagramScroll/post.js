class Post {
  constructor(puzzle, height) {
    this.originalY = height;
    this.y = height;
    this.x = windowWidth/2-postWidth/2;
    this.pictureHeight = 495;
    this.pictureWidth = 490;
    this.pictureX = this.x + ((postWidth - this.pictureWidth)/2);
    this.pictureY =  this.y+63;
    this.image = images[floor(random(0, images.length))];
    this.puzzle = puzzle;
    this.clicked = 0;
    this.user = floor(random(0, data.users.length));
    this.userLiked = floor(random(0, data.users.length));
    this.likedNumber = floor(random(5, 500))
    this.userComment = floor(random(0, data.users.length));
    this.comment = floor(random(0, data.comments.length));
    this.caption = floor(random(0, data.captions.length));
    this.panda = floor(random(0, gifs.length));

    if (this.puzzle) {
      amountOfPuzzles++;
      this.solved = false;
      this.difficulty = difficulty;

      //puzzle creator
      this.puzzleMatrix = new Array(this.difficulty);
      var n = 0;
      for (var i = 0; i < this.difficulty; i++) {
        this.puzzleMatrix[i] = new Array(this.difficulty);
        for (var j = 0; j < this.difficulty; j++) {
          n++;
          var tempPuzzlePiece = new PuzzlePiece(
            this.pictureX + (this.pictureWidth/this.difficulty*i),
            this.pictureY + (this.pictureHeight/this.difficulty*j),
            this.pictureWidth/this.difficulty,
            this.pictureHeight/this.difficulty,
            n,
            this.image,
            this.image.width/this.difficulty*i,
            this.image.height/this.difficulty*j,
            this.image.width/this.difficulty,
            this.image.height/this.difficulty);
          this.puzzleMatrix[i][j] = tempPuzzlePiece;
        }
      }

      for (i = 0; i < 100; i++) {
        var random1x = floor(random(0, this.difficulty));
        var random1y = floor(random(0, this.difficulty));
        var random2x = floor(random(0, this.difficulty));
        var random2y = floor(random(0, this.difficulty));
        [this.puzzleMatrix[random1x][random1y].originalY, this.puzzleMatrix[random2x][random2y].originalY] = [this.puzzleMatrix[random2x][random2y].originalY, this.puzzleMatrix[random1x][random1y].originalY];
        [this.puzzleMatrix[random1x][random1y].originalX, this.puzzleMatrix[random2x][random2y].originalX] = [this.puzzleMatrix[random2x][random2y].originalX, this.puzzleMatrix[random1x][random1y].originalX];
      }
    }
  }

  update() {
    this.y = this.originalY - scrollOffset;
    this.pictureY = this.originalY + 63 - scrollOffset;
    if (this.clicked > difficulty * 2 - 1 && !this.solved) {
      this.solved = true;
      amountSolved++;
    }
  }

  picture() {

    //post background
    image(instBackground, this.x, this.y, postWidth, postHeight);

    //picture or puzzle
    if (this.puzzle) {
      for (var i = 0; i < this.difficulty; i++) {
          for (var j = 0; j < this.difficulty; j++) {
            this.puzzleMatrix[i][j].update();
            this.puzzleMatrix[i][j].show();
        }
      }

      image(gifs[this.panda], this.x + 400, this.y + 565, 40, 40);

      for (var i = 0; i < this.difficulty; i++) {
        for (var j = 0; j < this.difficulty; j++) {
          if (this.puzzleMatrix[i][j].dragging) {
            this.puzzleMatrix[i][j].show();
          }
        }
      }
    }
    else {
      image(this.image, this.pictureX, this.pictureY, this.pictureWidth, this.pictureHeight);
    }
  }

  text() {
    //css
    textSize(22);
    fill(0);

    //profilename (user)
    text(data.users[this.user].user, this.x + 80, this.y + 35);

    //post caption (same user) (caption)
    textSize (18)
    text(data.users[this.user].user + " " + data.captions[this.caption].caption, this.x + 20, this.y + 655);

    //liked by (random number)(user)
    text("Liked by " + data.users[this.userLiked].user + " and " + this.likedNumber + " others", this.x + 20, this.y + 630);

    //comments (user)(comment)
    text(data.users[this.userComment].user + " " + data.comments[this.comment].comment, this.x + 20, this.y + 710);

    //view all comments (random number)
    fill(155);
    text("View all comments", this.x + 20, this.y + 685);



    //add comment...
    image(profile, this.x + 20, this.y + 725, 35, 35);
    text("Add a comment", this.x + 70, this.y + 750);

    //on day ago
    textSize(12);
    text("1 day ago", this.x + 20, this.y + 785)

  }

  drag() {
    closest = 10000;
    lastClickedPiece = createVector();
    this.clicked++;
    for (var i = 0; i < this.difficulty; i++) {
      for (var j = 0; j < this.difficulty; j++) {
        var d = dist(mouseX, mouseY, this.puzzleMatrix[i][j].x + this.puzzleMatrix[i][j].w/2, this.puzzleMatrix[i][j].y + this.puzzleMatrix[i][j].h/2);
        if (d < closest) {
          closest = d;
          lastClickedPiece.x = i;
          lastClickedPiece.y = j;
        }
      }
    }
    if (mouseX > this.pictureX && mouseX < this.pictureX + this.pictureWidth && mouseY > this.pictureY && mouseY < this.pictureY + this.pictureHeight) {
      this.puzzleMatrix[lastClickedPiece.x][lastClickedPiece.y].offsetX = this.puzzleMatrix[lastClickedPiece.x][lastClickedPiece.y].x - mouseX;
      this.puzzleMatrix[lastClickedPiece.x][lastClickedPiece.y].offsetY = this.puzzleMatrix[lastClickedPiece.x][lastClickedPiece.y].y - mouseY;
      this.puzzleMatrix[lastClickedPiece.x][lastClickedPiece.y].dragging = true;
    }
  }

  release() {
    if (mouseX > this.pictureX && mouseX < this.pictureX + this.pictureWidth &&  mouseY > this.pictureY && mouseY < this.pictureY + this.pictureHeight) {
      var releasedOn = createVector(i, j);
      var closest = 10000;
      for (var i = 0; i < this.difficulty; i++) {
        for (var j = 0; j < this.difficulty; j++) {
          var d = dist(mouseX, mouseY, this.puzzleMatrix[i][j].x + this.puzzleMatrix[i][j].w/2, this.puzzleMatrix[i][j].y + this.puzzleMatrix[i][j].h/2);
          if (d < closest) {
            closest = d;
            releasedOn.x = i;
            releasedOn.y = j;
          }
        }
      }
      [this.puzzleMatrix[lastClickedPiece.x][lastClickedPiece.y].originalY, this.puzzleMatrix[releasedOn.x][releasedOn.y].originalY] = [this.puzzleMatrix[releasedOn.x][releasedOn.y].originalY, this.puzzleMatrix[lastClickedPiece.x][lastClickedPiece.y].originalY];
      [this.puzzleMatrix[lastClickedPiece.x][lastClickedPiece.y].originalX, this.puzzleMatrix[releasedOn.x][releasedOn.y].originalX] = [this.puzzleMatrix[releasedOn.x][releasedOn.y].originalX, this.puzzleMatrix[lastClickedPiece.x][lastClickedPiece.y].originalX];
    }
    this.puzzleMatrix[lastClickedPiece.x][lastClickedPiece.y].dragging = false;
  }
}
