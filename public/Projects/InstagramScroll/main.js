var scrollOffset = 0;
var img;
var profile;
var posts = [];
var images = [];
var gifs = [];
var puzzles = [];
var generated = [0];
var lastClicked;
var lastClickedPiece;
var closest;
var firstClick = true;
var amountOfImg = 59;

var amountOfPuzzles = 0;
var amountSolved = 0;
var difficulty  = 2;
var frequency = 0;
var data;


var postWidth = 500;
var postHeight = 800;

function preload() {
  for (i = 0; i < 5; i++) {
      tempgif = loadImage('gifs/panda' + i + '.gif');
      tempgif.delay(250);
      gifs.push(tempgif);
  }

  for (i = 0; i < amountOfImg; i++) {
    tempImg = loadImage('images/'+ i +'.png');
    images.push(tempImg);
  }
  data = loadJSON('content.json');
}

function setup() {
  var cnv = createCanvas(windowWidth, windowHeight);
  cnv.style('display', 'block');

  instBackground = loadImage('instBackground.png')
  img = loadImage('meme.jpg');
  profile = loadImage('profile.png');

  for (i = 0; i < 10; i++) {
    puzzle = false
    height = i*postHeight;
    tempPost = new Post(puzzle, height);
    posts.push(tempPost);
  }
}

function draw() {
  background(240);

  //prints all posts
  for (i = 0; i < posts.length; i++) {
    posts[i].update();
    posts[i].picture();
    posts[i].text();
  }
}

function mouseWheel(event) {
  scrollOffset += (event.delta/2);
  if (scrollOffset < 0) {
    scrollOffset = 0;
  }

  if ((scrollOffset + postHeight + 200) % (postHeight*10) == 0 && !generated.includes(scrollOffset)) {
    generated.push(scrollOffset);
    createPosts();
  }
}

function mousePressed() {
  if (amountOfPuzzles > 0) {
    if (firstClick) {
      firstClick = false;
      var closest = 10000;
      lastClicked;
      for (i = 0; i < puzzles.length; i++) {
        var d = dist(mouseX, mouseY, puzzles[i].x + postWidth/2, puzzles[i].y + postHeight/2);
        if (d < closest) {
          closest = d;
          lastClicked = i;
        }
      }
      puzzles[lastClicked].drag();
    }
  }
}

function mouseReleased() {
  if (amountOfPuzzles > 0) {
    puzzles[lastClicked].release();
    firstClick = true;
  }
}

function createPosts() {
  //Creates x new posts with y puzzles (based on whether or not prior puzzles where solved)
  //with z complexsity (based on how many puzzles have been solved in all)

  //frecuency algorithm
  if (amountSolved > 0) {
    frequency = frequency - amountSolved;
    difficulty++
  }
  else {
    frequency++
  }

  //frecuency cap
  if (frequency < 1) {
    frequency = 1;
  }
  else if (frequency > 10) {
    frequency = 10;
  }

  //difficulty cap
  if (difficulty > 6) {
    difficulty = 6;
  }

  amountSolved = 0;

  //random post chooser for puzzles
  var puzzleNumber = [];
  for (i = 0; i < frequency; i++) {
    tempNumber = floor(random(0,10));
    if (puzzleNumber.includes(tempNumber)) {
      i--;
    }
    else {
      puzzleNumber.push(tempNumber)
    }
  }

  //post creator
  for (i = 0; i < 10; i++) {
    if (puzzleNumber.includes(i)) {
      puzzle = true;
    }
    else {
      puzzle = false;
    }

    height = scrollOffset+postHeight+200+(i*postHeight);
    tempPost = new Post(puzzle, height);
    posts.push(tempPost);
    if (puzzle) {
      puzzles.push(tempPost);
    }
  }
}
