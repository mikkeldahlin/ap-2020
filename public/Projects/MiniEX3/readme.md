![ScreenShot](ss.png)

https://mikkeldahlin.gitlab.io/ap-2020/Projects/MiniEX3/

![ScreenShot](trobber.png)

The goal for me with this MiniEx was to make a trobber that looked visually like Winnies own trobber but did not use the trick with applying alpha over the layers of circles. I felt while it was a smart fix to make the trobber that way, it also limited its use in the sense that it cannot be implemented within a program that uses a 255-alpha background. 
I used a for loop to make draw numberOfCircles(8) and uses the rotate function with 360/numberOfCircles)*i to place the circles differently for each loop, then add 360/numberOfCircles*frameCount to rotate the placement of each circle one position for each frame.
To make the illusion that it is in fact not 9 circles but one with an after image, I use the fill function withing the loop, using 255/numberOfCircles*I as the alpha argument. 



