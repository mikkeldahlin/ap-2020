let numberOfCircles = 8;
let timer = 1;
let dots = ['.', '..', '...']
let currentDots = 0

function setup() {
  createCanvas(windowWidth, windowHeight);
  frameRate(24);
}

function draw() {
  background(0);

  trobber();

  waitText();
}

function trobber() {
  noStroke();
  for (var i = 1; i < numberOfCircles+1; i++) {
      push();
      translate(mouseX, mouseY);
      rotate(radians((360/numberOfCircles)*i+360/numberOfCircles*frameCount));
      fill(255, 255/numberOfCircles*i);
      circle(35, 0, 22);
      pop();
    }
}

function waitText() {
  push();
    textAlign(CENTER)
    textSize(75);
    fill(50, 255, 50);
    text('Plz w8 4ever' + dots[currentDots], windowWidth/2, windowHeight/3);

    textAlign(LEFT);

    if (timer% 10 == 0) {
      currentDots++;
      timer = 1;
    } else {
      timer++;
    }

    if (currentDots==dots.length) {
      currentDots=0;
    }

  pop();
}
