//Navigation
var currentRoom = 0;

//Time
var currentDay = 1;
var currentWeekDay = ['Monday', 'Tuesday', 'Wednesday ', 'Thursday ', 'Friday ', 'Saturday', 'Sunday']

//Object vars
var currentSong = 0;
var lamp1on = false;
var lamp2on = false;

var roomTemp = 22;
var alarmChange = false;

//Backgrounds
var bedroombackground;
var newsscreen = [];
var socialmedia = [];
var outsideview = [];
var outsidepreview = [];

//Interaction vars
var newsChecked = false;
var computerChecked = false;
var windowChecked = false;

function preload() {
  bedroombackground = loadImage('pics/bedroom.png');
  livingroombackground = loadImage('pics/livingroom.png');
    fridgeBackground = loadImage('pics/fridge/fridge_background.png');
  song1 = loadSound('songs/Song1.mp3');
  song2 = loadSound('songs/Song2.mp3');
  song3 = loadSound('songs/Song3.mp3');
  alarmSound = loadSound('songs/alarm.mp3');
  no5 = loadSound('songs/no5.mp3');

  for (i = 1; i < 8; i++) {
    socialmedia[i] = loadImage('pics/socialmedia/media' + i + '.png')
    newsscreen[i] = loadImage('pics/news/news' + i + '.png')
    outsideview[i] = loadImage('pics/outside/window' + i + '.png')
    outsidepreview[i] = loadImage('pics/outside/window' + i + 'preview.png')
  }
}

function setup() {
  var cnv = createCanvas(1280, 720);
  cnv.style('display', 'block');

  createAllObjects();
  showHideObjects();
}

function draw() {

  switch(currentRoom) {
    case 0:
      bedroom();
      break;
    case 1:
      livingroom();
      break;
    case 2:
      view();
      break;
    case 3:
      television();
      break;
    case 4:
      pcscreen();
      break;
  }
}
