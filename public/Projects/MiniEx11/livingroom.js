function livingroom() {
  background(livingroombackground);

  if (!lamp2on && currentDay < 3) {
    background(0, 200);
  }

  //Fride styling (text and background)
  image(fridgeBackground, 407, 150, 250, 505)

  push();
    fill(254, 255, 200);
    textSize(12);
    text(currentWeekDay[currentDay-1], 562, 250);
  pop();

}

// LAMP --------------------------------------------------------------

function lamp2click() {
  lamp2on = !lamp2on;
  if (currentDay > 2) {
    lamp2.attribute('title','MAGPIE: I got the light for you')
  }
}

// SPEAKERS ------------------------------------------------------

function leftSpeakerclick() {
  if (currentDay < 3) {
    currentSong--;
    if (currentSong < 0) {
      currentSong = 3;
    }
    playSong();
  }
  else {
    playClassicalSong();
  }
}

function rightSpeakerclick() {
  if (currentDay < 3) {
      currentSong++;
    if (currentSong > 3) {
      currentSong = 0;
    }
    playSong();
  }
  else {
    playClassicalSong();
  }
}

//TABLET------------------------------------------------------

function helloWorld() {
  tablethw.show();
  tablet.hide();
}

function goodbyeWorld() {
  tablethw.hide()
  tablet.show()
}

//FRIDGE------------------------------------------------------


function fridgeDoorOpen(){
  fridge.hide();
  fridgeeyes.hide();
  if (currentDay < 6) {
    fridgeopen1.show();
  }
  else {
    fridgeopen2.show();
  }

}

function fridgeDoorClose(){
  fridgeopen1.hide();
  fridgeopen2.hide();
  fridge.show();
  if (currentDay > 5) {
    fridge.hide();
    fridgeeyes.show()
  }
}

//SONG CONTROL ------------------------------------------------------

function playSong() {
  switch(currentSong) {
    case 0:
      song1.stop()
      song2.stop()
      song3.stop()
      break;
    case 1:
      song1.play()
      song2.stop()
      song3.stop()
      break;
    case 2:
      song1.stop()
      song2.play()
      song3.stop()
      break;
    case 3:
      song1.stop()
      song2.stop()
      song3.play()
      break;
  }
}

function playClassicalSong() {
  if (no5.isPlaying()) {
    no5.stop();
  }
  else {
    no5.play();
  }
}

//NAVIGATION TO OTHER ROOMS ------------------------------------------------------

function tvclick() {
  newsChecked = true;
  currentRoom = 3;
  showHideObjects();
}

function goToBedroom() {
  currentRoom = 0;
  showHideObjects();
}
