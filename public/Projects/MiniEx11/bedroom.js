function bedroom() {
background(bedroombackground);

  if (!lamp1on && currentDay < 3) {
    background(0, 200);
  }

  push();
    fill(100, 231, 255);
    textSize(20);
    text(roomTemp + '°', 1135, 228);
  pop();

  image(outsidepreview[currentDay], 60, 140, 380, 240);
}

//LAMP ---------------------------------------------------------------

function lamp1click() {
  lamp1on = !lamp1on;
  if (currentDay > 2) {
    lamp1.attribute('title','MAGPIE: I got the light for you')
  }
}

//CURTAIN---------------------------------------------------------------

function curtainClick(){
  curtainup.show();
  curtain.hide();
  if (currentDay > 4) {
    setTimeout(curtainUpClick, 1500);
  }
}

function curtainUpClick(){
  if (currentRoom == 0) {
    curtainup.hide();
    curtain.show();
  }
}


//ALARM ---------------------------------------------------------------

function alarmClicked() {
  if (currentDay < 5) {
    alarmSound.stop();
    alarmclock.show();
  }
  clearInterval(alarmAnimationInterval);
  alarmclockearly.hide();
}

function alarmAnimation() {
  if (alarmChange) {
    alarmclock.hide();
    alarmclockearly.show();
  }
  else {
    alarmclock.show();
    alarmclockearly.hide();
  }

  if (currentRoom != 0) {
    alarmclock.hide();
    alarmclockearly.hide();
  }

  alarmChange = !alarmChange;
}

//BED -----------------------------------------------------------------

function bedclick() {
  if (newsChecked && computerChecked && windowChecked) {
    newsChecked = false;
    computerChecked = false;
    windowChecked = false;
  }
  else {
    if (newsChecked == false) {
      message.show();
      messageText.show();
      messageText2 = createDiv('watch the news on TV');
      messageText2.position(220, 430);
    }
    else if (computerChecked == false) {
      message.show();
      messageText.show();
      messageText2 = createDiv('check the computer');
      messageText2.position(220, 430);
    }
    else if (windowChecked == false) {
      message.show();
      messageText.show();
      messageText2 =createDiv('take a look outside');
      messageText2.position(220, 430);
    }
    setTimeout(removeThought, 4000);
    return;
  }

  currentDay++;

  showHideObjects();

  roomTemp = 22;

  if (currentDay == 3) {
    song1.stop()
    song2.stop()
    song3.stop()
  }

  sleepanimation.show();
  if (currentDay == 8) {
    sleepText = createDiv('The End');
    currentDay = 7;
    noLoop();
  }
  else {
    setTimeout(hideSleep, 1500);
    sleepText = createDiv('Day ' + currentDay);
    alarmSound.play();
    if (currentDay < 5) {
      alarmAnimationInterval = setInterval(alarmAnimation, 500);
    }
    else {
      alarmclock.hide();
      alarmclockeyes.show();
    }
  }
  sleepText.position(585, 320);
  sleepText.style('color', 'white')
  sleepText.style('font-size', '45px')

}

function removeThought() {
  message.hide();
  messageText.hide();
  messageText2.hide();
}

function hideSleep() {
  sleepanimation.hide();
  sleepText.remove();
}

//GO TO OTHER ROOMS -------------------------------------------------

function goToLivingroom() {
  currentRoom = 1;
  showHideObjects();
}

function goToComputer() {
  computerChecked = true;
  currentRoom = 4;
  showHideObjects();
}

function windowclick() {
  windowChecked = true;
  currentRoom = 2;
  showHideObjects();
}

//TERMOSTAT ----------------------------------------------------------

function turnDownTemp() {
  roomTemp--;
  if (roomTemp < 15) {
    roomTemp = 15;
  }
  else if (currentDay > 1) {
    setTimeout(tempRestrictionUp, 1000);
  }
}

function  turnUpTemp() {
  roomTemp++;
  if (roomTemp > 25) {
    roomTemp = 25;
  }
  else if (currentDay > 1) {
    setTimeout(tempRestrictionDown, 1000);
  }
}

function tempRestrictionUp() {
  roomTemp++;
}


function tempRestrictionDown() {
  roomTemp--;
}
