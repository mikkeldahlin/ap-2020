function createAllObjects() {

//BEDROOM --------------------------------------------------------------

  lamp1 = createImg('pics/lamp.png');
  lamp1.position(580, 0);
  lamp1.size(120, 180);
  lamp1.mouseClicked(lamp1click);
  lamp1.style('cursor', 'pointer');
  lamp1.attribute('title','Light switch')

//BED -------------------------------------------------------------------

  bed = createImg('pics/bed.png');
  bed.position(0, 425);
  bed.size(450, 250);
  bed.mouseClicked(bedclick);
  bed.style('cursor', 'pointer');
  bed.style('allign', 'center');
  bed.attribute('title','Sleep')

//NAVIGATION TO LIVINGROOM  --------------------------------------------------

  toLivingroom = createImg('pics/door1.png');
  toLivingroom.position(1192, 90);
  toLivingroom.size(100, 585)
  toLivingroom.mouseClicked(goToLivingroom);
  toLivingroom.style('cursor', 'pointer');
  toLivingroom.attribute('title','Go to livingroom')

//PC  -------------------------------------------------------------------

  pc = createImg('pics/computer.png');
  pc.position(800, 230);
  pc.size(200, 130);
  pc.mouseClicked(goToComputer);
  pc.style('cursor', 'pointer');
  pc.attribute('title','Check social media')

  pceyes = createImg('pics/computereyes.gif');
  pceyes.position(800, 230);
  pceyes.size(200, 130);
  pceyes.mouseClicked(goToComputer);
  pceyes.style('cursor', 'pointer');
  pceyes.attribute('title','Check computer')

//TEMPERATURE  ----------------------------------------------------------------

  tempminus = createImg('pics/minus.png');
  tempminus.position(1123, 244);
  tempminus.size(25, 13);
  tempminus.mouseClicked(turnDownTemp);
  tempminus.style('cursor', 'pointer');
  tempminus.attribute('title','Temperature down')


  tempplus = createImg('pics/plus.png');
  tempplus.position(1148, 244);
  tempplus.size(24, 13);
  tempplus.mouseClicked(turnUpTemp);
  tempplus.style('cursor', 'pointer');
  tempplus.attribute('title','Temperature up')
//ALARM ----------------------------------------------------------------------

  alarmclock = createImg('pics/alarmclock.png');
  alarmclock.position(470, 355);
  alarmclock.size(65, 35);
  alarmclock.mouseClicked(alarmClicked);
  alarmclock.style('cursor', 'pointer');
  alarmclock.attribute('title','Turn off alarm')

  alarmclockearly = createImg('pics/alarmclockearly.png');
  alarmclockearly.position(470, 355);
  alarmclockearly.size(65, 35);
  alarmclockearly.mouseClicked(alarmClicked);
  alarmclockearly.style('cursor', 'pointer');
  alarmclockearly.attribute('title','Turn off alarm')

  alarmclockeyes = createImg('pics/alarmclockeyes.gif');
  alarmclockeyes.position(470, 355);
  alarmclockeyes.size(65, 35);
  alarmclockeyes.mouseClicked(alarmClicked);
  alarmclockeyes.style('cursor', 'pointer');
  alarmclockeyes.attribute('title', 'MAGPIE: Get up')

//WINDOW -----------------------------------------------------------------

  invisiblewindow = createImg('pics/outside/invisiblewindow.png');
  invisiblewindow.position(60, 140);
  invisiblewindow.size(380, 240);
  invisiblewindow.mouseClicked(windowclick);
  invisiblewindow.style('cursor', 'pointer');
  invisiblewindow.attribute('title','Look outside')

  curtain = createImg('pics/outside/curtain.png');
  curtain.position(60, 140);
  curtain.size(380, 240);
  curtain.mouseClicked(curtainClick);
  curtain.style('cursor', 'pointer');
  curtain.attribute('title','Roll curtain up')

  curtainup = createImg('pics/outside/curtainup.png');
  curtainup.position(60, 140);
  curtainup.size(380, 30);
  curtainup.mouseClicked(curtainUpClick);
  curtainup.style('cursor', 'pointer');
  curtainup.attribute('title','Roll curtain down')

// WHAT TO DO MESSAGE -----------------------------------------------

  message = createImg('pics/thought.png');
  message.position(140, 350);
  message.size(300, 200);

  messageText = createDiv('Hmm... Its still early, maybe i should');
  messageText.position(170, 410);

  messageText2 = createDiv('');

// SLEEPANIMATION----------------------------------------------------

  sleepanimation = createImg('pics/sleep.png');
  sleepanimation.position(0, 0);
  sleepanimation.size(width, height);

//LIVINGROOM ---------------------------------------------------------

  lamp2 = createImg('pics/lamp.png');
  lamp2.position(640, -20);
  lamp2.size(140, 160);
  lamp2.mouseClicked(lamp2click);
  lamp2.style('cursor', 'pointer');
  lamp2.attribute('title','Light switch')

// TV & SPEAKERS ------------------------------------------------------

  tv = createImg('pics/tv.png');
  tv.position(825, 305);
  tv.size(300, 150);
  tv.mouseClicked(tvclick);
  tv.style('cursor', 'pointer');
  tv.attribute('title','Check the news')

  tveyes = createImg('pics/tveyes.gif');
  tveyes.position(825, 305);
  tveyes.size(300, 150);
  tveyes.mouseClicked(tvclick);
  tveyes.style('cursor', 'pointer');
  tveyes.attribute('title','Check the news')

  lspeaker = createImg('pics/lspeaker.png');
  lspeaker.position(790, 305);
  lspeaker.size(35, 150);
  lspeaker.mouseClicked(leftSpeakerclick);
  lspeaker.style('cursor', 'pointer');
  lspeaker.attribute('title', 'Previous song')

  rspeaker = createImg('pics/rspeaker.png');
  rspeaker.position(1125, 305);
  rspeaker.size(35, 150);
  rspeaker.mouseClicked(rightSpeakerclick);
  rspeaker.style('cursor', 'pointer');
  rspeaker.attribute('title', 'Next song')

//TABLET  -------------------------------------------------------------------

  tablet = createImg('pics/tablet.png')
  tablet.position(890, 582)
  tablet.size(115, 70);
  tablet.mouseClicked(helloWorld)
  tablet.style('cursor', 'pointer')
  tablet.attribute('title', 'MAGPIE: Hello')

  tablethw = createImg('pics/tablethw.png')
  tablethw.position(890, 582);
  tablethw.size(115, 70);
  tablethw.mouseClicked(goodbyeWorld);
  tablethw.style('cursor', 'pointer')
  tablethw.attribute('title', 'MAGPIE: Hello')

  tableteyes = createImg('pics/tableteyes.png')
  tableteyes.position(890, 582);
  tableteyes.size(115, 70);
  tableteyes.style('cursor', 'pointer')
  tableteyes.attribute('title', 'MAGPIE: Hello')

//CLOCK  -------------------------------------------------------------------

  clockearly = createImg('pics/clockearly.png')
  clockearly.position(1055, 110)
  clockearly.size(70,70)
  clockearly.attribute('title', 'Its still early')

  clocklate = createImg('pics/clocklate.png')
  clocklate.position(1055, 110)
  clocklate.size(70,70)
  clocklate.attribute('title', 'Time for bed')

//FRIDGE -------------------------------------------------------------------

  fridge = createImg('pics/fridge/fridge.png');
  fridge.position(407, 150);
  fridge.size(250, 505);
  fridge.mouseClicked(fridgeDoorOpen)
  fridge.style('cursor', 'pointer')
  fridge.attribute('title','Open the fridge')

  fridgeopen1 = createImg('pics/fridge/fridgeopen1.png');
  fridgeopen1.position(407, 150);
  fridgeopen1.size(360, 505);
  fridgeopen1.mouseClicked(fridgeDoorClose)
  fridgeopen1.style('cursor', 'pointer')
  fridgeopen1.attribute('title','Close the fridge')

  fridgeopen2 = createImg('pics/fridge/fridgeopen2.png');
  fridgeopen2.position(407, 150);
  fridgeopen2.size(360, 505);
  fridgeopen2.mouseClicked(fridgeDoorClose)
  fridgeopen2.style('cursor', 'pointer')
  fridgeopen2.attribute('title','Close the fridge')

  fridgeeyes = createImg('pics/fridge/fridgeeyes.gif');
  fridgeeyes.position(407, 150);
  fridgeeyes.size(252, 505);
  fridgeeyes.mouseClicked(fridgeDoorOpen)
  fridgeeyes.style('cursor', 'pointer')
  fridgeeyes.attribute('title','MAGPIE: I bought groceries')

//NAVIGATION TO BEDROOM -----------------------------------------------------

  toBedroom = createImg('pics/door2.png');
  toBedroom.position(-15, 80);
  toBedroom.size(100, 585);
  toBedroom.mouseClicked(goToBedroom);
  toBedroom.style('cursor', 'pointer');
  toBedroom.attribute('title','Go to bedroom')

// OTHER ROOMS --------------------------------------------------------------

  invisibleBackToBedroom = createImg('pics/outside/invisiblewindow.png');
  invisibleBackToBedroom.position(0, 0);
  invisibleBackToBedroom.size(width, height);
  invisibleBackToBedroom.style('cursor', 'pointer');
  invisibleBackToBedroom.attribute('title','Return to bedroom')

  invisibleBackToLivingroom = createImg('pics/outside/invisiblewindow.png');
  invisibleBackToLivingroom.position(0, 0);
  invisibleBackToLivingroom.size(width, height);
  invisibleBackToLivingroom.style('cursor', 'pointer');
  invisibleBackToLivingroom.attribute('title','Return to livingroom')

}

function showHideObjects () {
  lamp1.hide();
  bed.hide();
  tv.hide();
  lspeaker.hide();
  rspeaker.hide();
  toLivingroom.hide();
  toBedroom.hide();
  pc.hide();
  tempminus.hide();
  tempplus.hide();
  alarmclock.hide();
  sleepanimation.hide();
  tablet.hide();
  tablethw.hide();
  lamp2.hide();
  clockearly.hide();
  fridge.hide();
  fridgeopen1.hide();
  fridgeopen2.hide();
  invisiblewindow.hide();
  alarmclockearly.hide();
  alarmclockeyes.hide();
  curtain.hide();
  curtainup.hide();
  fridgeeyes.hide();
  tveyes.hide();
  pceyes.hide();
  tableteyes.hide();
  clocklate.hide();
  invisibleBackToBedroom.hide();
  invisibleBackToLivingroom.hide();
  messageText.hide();
  messageText2.hide();
  message.hide();

  switch (currentRoom) {
    case 0: //BEDROOM
      lamp1.show();
      bed.show();
      toLivingroom.show();
      pc.show()
      if (currentDay > 3) {
        pc.hide();
        pceyes.show();
      }
      tempminus.show();
      tempplus.show();
      alarmclock.show();
      curtainup.show();
      if (currentDay > 4) {
        alarmclock.hide();
        alarmclockeyes.show();
        curtainup.hide();
        curtain.show();
      }
      invisiblewindow.show();
      break;
    case 1: //LIVINGROOM
      lamp2.show();
      tv.show();
      if (currentDay > 4) {
        tv.hide();
        tveyes.show();
      }
      lspeaker.show();
      rspeaker.show();
      toBedroom.show();
      tablet.show();
      if (currentDay > 1) {
        tablet.hide();
        tableteyes.show();
      }
      clockearly.show();
      if (newsChecked && computerChecked && windowChecked) {
          clockearly.hide();
          clocklate.show();
        }
      fridge.show();
      if (currentDay > 5) {
        fridge.hide();
        fridgeeyes.show();
      }
      break;
    case 2: //VIEW
      invisibleBackToBedroom.show();
      break;
    case 3: //TELEVISION
      invisibleBackToLivingroom.show();
      break;
    case 4: //COMPUTER
      invisibleBackToBedroom.show();
      break;
  }
}
