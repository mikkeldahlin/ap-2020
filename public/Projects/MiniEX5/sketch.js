let posx = 600;
let posy = 790;

function preload() {
  //first line
  gimp = loadImage("gimp.png");
  imdb = loadImage("imdb.png");
  linkedin = loadImage("linkedin.png");
  messenger = loadImage("messenger.png");
  youtube = loadImage("youtube.png");
  //secound line
  audible = loadImage("audible.png");
  drev = loadImage("drev.png");
  github = loadImage("github.png");
  gitlab = loadImage("gitlab.png");
  whatsapp = loadImage("whatsapp.png");
  //third line
  linux = loadImage("linux.png");
  deepmind = loadImage("deepmind.png");
  instagram = loadImage("instagram.png");
  nokia = loadImage("nokia.png");
  twitter = loadImage("twitter.png");
  //Fouth line
  wordpress = loadImage("wordpress.png");
  bungie = loadImage("bungie.png");
  jigsaw = loadImage("jigsaw.png");
  friendfeed = loadImage("friendfeed.png");
  buisniessinsider = loadImage("buisniessinsider.png");

  //Fitht line
  twitch = loadImage("twitch.png");
  android = loadImage("android.png");
  ocolus = loadImage("ocolus.png");
  duckduckgo = loadImage("duckduckgo.png");
  skype = loadImage("skype.png");

}

function setup() {
  createCanvas(1200, 800);
}

function draw() {
  background(255);
  noStroke();

  //walls
  fill(0);
  rectMode(CENTER);
  for (i = 0; i < 6; i++){
    rect(i*240, 400, 20, 800);
  }

  fill(255);
  for (i = 0; i < 6; i++){
    rect(600, i*150+25, 1100, 50);
  }

  //image placment
  //first line
  imageMode(CENTER);
  image(gimp, 600, 700, 150, 100);
  image(imdb, 120, 700, 150, 100);
  image(youtube, 360, 700, 150, 100);
  image(messenger, 1080, 700, 80, 80);
  image(linkedin, 840, 700, 80, 80);

  //secound line
  image(drev, 600, 550, 100, 80);
  image(gitlab, 120, 550, 80, 80);
  image(github, 360, 550, 100, 80);
  image(whatsapp, 1080, 550, 80, 80);
  image(audible, 840, 550, 80, 80);

  //third line
  image(twitter, 600, 400, 140, 100);
  image(deepmind, 120, 400, 140, 80);
  image(nokia, 360, 400, 120, 100);
  image(instagram, 1080, 400, 80, 80);
  image(linux, 840, 400, 120, 100);

  //Fouth line
  image(buisniessinsider, 600, 250, 100, 80);
  image(bungie, 120, 250, 120, 120);
  image(wordpress, 360, 250, 160, 120);
  image(jigsaw, 1080, 250, 140, 70);
  image(friendfeed, 840, 250, 100, 80);

  //Fith line
  image(twitch, 600, 100, 120, 80);
  image(android, 120, 100, 100, 100);
  image(skype, 360, 100, 110, 90);
  image(duckduckgo, 1080, 100, 100, 80);
  image(ocolus, 840, 100, 100, 80);

  //player
  fill(127);
  rect(posx, posy, 30, 30);


  //movement and movement restriction
  if (keyIsDown(RIGHT_ARROW)) {
    posx += +5;
  }

  if (keyIsDown(LEFT_ARROW)) {
    posx += -5;
  }

  if (keyIsDown(38)) {
    posy += -5;
  }

  if (keyIsDown(40)) {
    posy += 5;
  }

  if (posx > 1175) {
    posx = 1175;
  }

  if (posx < 25) {
    posx = 25;
  }

  if (posy > 785) {
    posy = 785;
  }

  if (posy < 15) {
    posy = 15;
  }

  //safe passage, collision reset
  //first line
  if (posx > 0 && posx < 480 && posy < 750 && posy > 650){
    posx = 600;
    posy = 775;
  }

  if (posx > 720 && posx < 1200 && posy < 750 && posy > 650){
    posx = 600;
    posy = 775;
  }

  //secound line
  if (posx > 240 && posx < 1200 && posy < 600 && posy > 500){
    posx = 600;
    posy = 775;
  }

  //third line
  if (posx > 0 && posx < 720 && posy < 450 && posy > 350){
    posx = 600;
    posy = 775;
  }

  if (posx > 960 && posx < 1200 && posy < 450 && posy > 350){
    posx = 600;
    posy = 775;
  }

  //fouth line
  if (posx > 0 && posx < 240 && posy < 300 && posy > 200){
    posx = 600;
    posy = 775;
  }

  if (posx > 480 && posx < 1200 && posy < 300 && posy > 200){
    posx = 600;
    posy = 775;
  }

  //fouth line
  if (posx > 0 && posx < 960 && posy < 150 && posy > 50){
    posx = 600;
    posy = 775;
  }
}
