class InvaderBullet {
  constructor(x, y) {
    this.x = x;
    this.y = y;
    this.speed = 3;
  }

  move() {
    this.y = this.y + this.speed;
  }

  show() {
    image(shot, this.x, this.y, 50, 50);
  }
}
