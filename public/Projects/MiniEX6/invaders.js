class Invaders {
  constructor(x, y, z) {
    this.x = x;
    this.y = y;
    this.z = z;
    this.xdir = - 0.33;
  }

  move() {
    this.x = (this.x + this.xdir);

    let r = floor(random(0, 750));
    if (r == 1) {
      tempInvaderBullet = new InvaderBullet(this.x, this.y);
      myInvaderBullets.push(tempInvaderBullet);
    }
    if (this.y >= 850) {
      lives = 0;
    }
  }

  show() {
    image(invader[this.z], this.x, this.y, 50, 50);
  }

  shiftdown() {
    this.xdir *= -1;
    this.y +=50;
  }
}
