class Explosion {
  constructor(x, y) {
    this.x = x;
    this.y = y;
    this.t = 30;
  }

  show() {
    image(explosion, this.x, this.y, 50, 50);

    this.t = this.t - 1;
    if ( this.t <= 0) {
      myExplosions.splice(this, 1);
    }
  }
}
