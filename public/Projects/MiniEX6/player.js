class Player {
  constructor(x_value) {
    this.x = 350;
    this.y = 840;
  }

  move() {
    if (mouseX < 680) { //bug fix for player staying in middle if mouse is moved too fast out of the canvas
      this.x = mouseX;
      if (this.x < 20) { //restrain so that player is able to move same amount to the left as it can to the right without exiting the canvas
        this.x = 20;
      }
    }
    else {
      this.x = 680;
    }
  }

  show() {
      image(player, this.x, this.y, 50, 50);
  }
}
