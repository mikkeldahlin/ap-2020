class Ufo {
  constructor(x, y, z) {
    this.x = 750;
    this.y = 75;
    this.xdir = - 1;
  }

  move() {
    this.x = (this.x + this.xdir);

    let r = floor(random(0, 25));
    if (r == 1) {
      tempInvaderBullet = new InvaderBullet(this.x, this.y);
      myInvaderBullets.push(tempInvaderBullet);
    }
  }

  show() {
    image(ufo, this.x, this.y, 50, 50);
  }
}
