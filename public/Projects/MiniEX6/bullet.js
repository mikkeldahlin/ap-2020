class Bullet {
  constructor(x) {
    this.x = x;
    this.y = height-25;
    this.speed = 3;
  }

  move() {
    this.y = this.y - this.speed;
  }

  show() {
    image(myshot, this.x, this.y, 50, 50);
  }
}

function mousePressed() {
  if (counter == 0) {
    shoot(); //see bullet
  }
}

function shoot() {
  if(mouseButton == LEFT) {
    let TempMouseX = mouseX;
    if (mouseX > 680) {
      TempMouseX = 680; //makes sure the bullet cannot be shoot outside of player movement
    }
    if (mouseX < 20) {
        TempMouseX = 20;
    }
    tempBullet = new Bullet(TempMouseX);
    myBullets.push(tempBullet);
    shotSound.play();
    counter = 30;
  }
}
