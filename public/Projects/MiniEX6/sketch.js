let myPlayer;
let myBullets = [];
let tempBullet;
let myInvaders = [];
let invader = [];
let tempExplosion;
let myExplosions = [];
let myInvaderBullets = [];
let tempInvaderBullet;
let lives = 3;
let counter = 0;
let score =  0;
let myUfo = [];
let tempUfo;
let highscore = 0;
let customfont;

function preload() { //loads all sprites
  player = loadImage("pics/player.png");
  ufo = loadImage("pics/ufo.png");
  shot = loadImage("pics/shot.gif");
  explosion = loadImage("pics/explosion.gif");
  myshot = loadImage("pics/myshot.png");

  for (i = 0; i < 9; i++) {
    invader[i] = loadImage('pics/invader' + i + '.gif')
  }
  customfont = loadFont('customfont.ttf');

  ost = loadSound('sound/OST.mp3');
  explosionSound = loadSound('sound/explosionSound.mp3');
  shotSound = loadSound('sound/shotSound.wav');
  hitSound = loadSound('sound/hitSound.wav');
  lostSound = loadSound('sound/lostSound.wav');
  ufoSound = loadSound('sound/ufoSound.mp3');
}

function setup() {
  createCanvas(700, 850);
  myPlayer = new Player();
  frameRate(60);
  imageMode(CENTER);
  fill(0, 255, 00);
  textFont(customfont);
  setInterval(makeUfo,20000); //set interval for UFOs to appear

  ost.play();

  let cookie = document.cookie; //read highscore cookie and if there is non sets its value to 0
  highscore = split(cookie, '=');
  if (highscore[1] == null){
    highscore[1] = 0;
  }

  for (i = 0; i < 9; i++) { //delays sprite animation
    invader[i].delay(5000);
  }

  for (i = 0; i < 91; i++) { //creates all invaders useing diffrent kinds for every 10th
    z = floor(i/10);
    myInvaders[i] = new Invaders(i*50+225-z*500, 100+z*50, z);
  }
}

function makeUfo() {
  tempUfo = new Ufo();
  myUfo.push(tempUfo);
  ufoSound.play();
  setTimeout(playUfoSound, 6500);
}

function playUfoSound() {
  ufoSound.play();
}

function draw() {
  background(0);
  cursor(CROSS, mouseX, mouseY);
  myPlayer.move();
  myPlayer.show();

//myBullets
  for (i = 0; i < myBullets.length; i++) {
    myBullets[i].move();
    myBullets[i].show();

    if (myBullets[i].y < 0) { //remove bullets if they exit map (for less computing)
      myBullets.splice(i, 1);
      score -= 10;
    }
  }
//counter for shot delay
  counter--;
  if (counter < 0) {
    counter = 0;
  }


//Invaders
  for (i = 0; i < myInvaders.length-1; i++) {
    myInvaders[i].move();
    myInvaders[i].show();

    for (j = 0; j < myBullets.length; j++) {
      let d = dist(myInvaders[i].x, myInvaders[i].y, myBullets[j].x, myBullets[j].y)
      if (d < 15) {
        tempExplosion = new Explosion(myInvaders[i].x, myInvaders[i].y);
        myExplosions.push(tempExplosion);
        myInvaders.splice(i, 1);
        myBullets.splice(j, 1);
        explosionSound.play();
        score += 50;
      }
    }

    if (myInvaders[i].x < 25 || myInvaders[i].x > 675) { //reverse direction of invaders and shifts them down each time one hits a side
      for (k=0; k<myInvaders.length; k++) {
        myInvaders[k].shiftdown();
      }
    }
  }

//Explosions
  for (i = 0; i < myExplosions.length; i++) {
    myExplosions[i].show();
  }

//invadesBullets
  for (i = 0; i < myInvaderBullets.length; i++) {
    myInvaderBullets[i].move();
    myInvaderBullets[i].show();

    d = dist(myInvaderBullets[i].x, myInvaderBullets[i].y, myPlayer.x, myPlayer.y)
    if (d < 15) {
      myInvaderBullets.splice(i, 1);
      lives --;
      score -= 100
      hitSound.play();
    }
    if (myInvaderBullets[i].y > 900) { //remove bullets if they exit map (for less computing)
      myInvaderBullets.splice(i, 1);
    }
  }

//ufo
  for (i = 0; i < myUfo.length; i++) {
    myUfo[i].move();
    myUfo[i].show();
    for (j = 0; j < myBullets.length; j++) {
      let d = dist(myUfo[i].x, myUfo[i].y, myBullets[j].x, myBullets[j].y)
      if (d < 20) {
        tempExplosion = new Explosion(myUfo[i].x, myUfo[i].y);
        myExplosions.push(tempExplosion);
        myBullets.splice(j, 1);
        score += 500;
        explosionSound.play();
      }
    }
  }

//score/lives
  textAlign(LEFT);
  textSize(34);
  if (score < 0) {
    score = 0;
  }
  text('Score:' + score, 10, 30);
  textAlign(RIGHT)
  text('Lives:' + lives, 690, 30);


//win
  if (myInvaders.length <= 1) {
    textAlign(CENTER);
    textSize(56);
    text('You Win', 350, 340);
    stop();
  }

//lose
  if (lives <= 0) {
    textAlign(CENTER);
    textSize(56);
    text('You Lose', 350, 340);
    lostSound.play();
    stop();
  }

//stops game
  function stop() {
    text('Your score:' + score, 350, 400);
    if (score >= highscore[1]) {
      highscore[1]  = score;
      document.cookie = "highscore=" + highscore[1] + "; expires=Thu, 31 Dec 2020 12:00:00 UTC";
    }
    text('Highscore:' + highscore[1], 350, 465);
    noLoop();
  }
}
