// Global vars ----------------------------------------------------------------
let player;
let vel;
let walls;
let jumping = false;
let curYpos;
let preYpos1;
let preYpos2;

// interchangeable vars --------------------------------------------------------
var gravity = 1;
var deceleration = 1;


// setup ----------------------------------------------------------------------
function setup() {

  var cnv = createCanvas(windowWidth, windowHeight);
  cnv.style('display', 'block');

  player = createSprite(30, windowHeight-30, 50, 50);

  walls = new Group()
  createWall(windowWidth/2, windowHeight-100, 100, windowHeight/4);
  createWall(windowWidth/1.5, windowHeight-200, 200, windowHeight/6);
  createWall(windowWidth/3, windowHeight-400, 150, windowHeight/10)
  createWall(windowWidth/5, windowHeight-600, 250, 100);
  createWall(windowWidth/1.5, windowHeight-700, 300, 50);
  createWall(windowWidth/2.5, 75, 100, 50);


  topwall = createSprite(0, 5, windowWidth*2, 10);
  topwall.shapeColor = 0;
  leftwall = createSprite(5, 0, 10, windowHeight*2);
  leftwall.shapeColor = 0;
  bottomwall = createSprite(0, windowHeight-5, windowWidth*2, 10);
  bottomwall.shapeColor = 0;
  rightwall = createSprite(windowWidth-5, 0, 10, windowHeight*2);
  rightwall.shapeColor = 0;
  walls.add(topwall);
  walls.add(bottomwall);
  walls.add(leftwall);
  walls.add(rightwall);
}

function createWall (x, y, x1, y2) {
  wall = createSprite(x, y, x1, y2);
  walls.add(wall)
}


function draw() {
  background(255);

// PLAYER ----------------------------------------------------------------------

//Movement-----------------------------------------------

if (keyIsDown(RIGHT_ARROW)) {
  player.velocity.x +=2;
}

if (keyIsDown(LEFT_ARROW)) {
  player.velocity.x -=2;
}

//Jump---------------------------------

  curYpos = player.position.y;

  if (curYpos == preYpos2) {
    jumping = false;
  }
  else {
    jumping = true;
  }

  preYpos1 = player.position.y;

  preYpos2 = preYpos1;

//Constrains and Deaccelaration -------------------------

  if (player.velocity.x < 0) {
    deceleration = 1;
  }
  else if (player.velocity.x > 0) {
    deceleration = -1;
  }

  if (player.velocity.x < 1 && player.velocity.x > -1) {
    deceleration= 0;
  }

  player.velocity.x += deceleration;

  player.velocity.x = constrain(player.velocity.x, -15, 15);

//Gravity -------------------------------------------------

  player.velocity.y += gravity;

  player.velocity.y = constrain(player.velocity.y, -30, 30);

  player.collide(walls);
  drawSprites();
}

function keyPressed() {
  if (keyCode === 32 && jumping === false) {
    player.velocity.y = -20;
  }
}

/*

*/
