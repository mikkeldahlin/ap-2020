//GOLBAL VARS -----------------------------------------------------------------
var gravity = 1;
var friction = 0.9;
var speed = 5;
var jump_height = 15;

var game_width = 1200;
var game_height = 500;

var player_width = 20;
var player_height = 40;

//NO TOUCH VARS
var collision;
var myWalls = [];

function setup() {
  createCanvas(game_width, game_height);
  myPlayer = new Player();
  createWall(0, game_height, game_width, 200);
  createWall(400,400,200,50);
  createWall(800,400,200,50);
}

function draw (){
  background(0);
  myPlayer.run();
  for (i = 0; i < myWalls.length; i++) {
    myWalls[i].run();
  }
}
