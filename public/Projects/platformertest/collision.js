
function collisionCheck(type1, type2) {
  collision = false;
  var x1 = floor(type1.position.x);
  var x2 = floor(type1.position.x + type1.width);
  var y1 = floor(type1.position.y + player_height);
  for (i = 0; i < type2.length; i++) {
    if (x1 > type2[i].position.x && x1 < type2[i].position.x + type2[i].width && y1 > type2[i].position.y-1 && y1 < type2[i].position.y + type2[i].width || x2 > type2[i].position.x && x2 < type2[i].position.x + type2[i].width && y1 > type2[i].position.y-1 && y1 < type2[i].position.y + type2[i].width) {
      collision = true;
      break;
    }
  }
  return collision;
}
