class Player {
  constructor() {
    this.position = createVector(0, game_height - player_height)
    this.velocity = createVector();
    this.jumping = false;
    this.width = player_width;
    this.height = player_height;
  }

  run() {
    this.move();
    this.update();
    this.display();
  }

  state() {
    collisionCheck(myPlayer, myWalls);
    if (collision) {
      this.jumping = false;
    }
  }

  move() {
    if (keyIsDown(RIGHT_ARROW)) {
      this.velocity.x +=2;
    }
    if (keyIsDown(LEFT_ARROW)) {
      this.velocity.x -=2;
    }
    if (keyIsDown(32)) {
      this.state();
      if (this.jumping === false) {
        this.jumping = true;
        this.velocity.y = -jump_height;
      }
    }
  }

  update() {
    this.velocity.y += gravity;
    this.position.y += this.velocity.y;

    this.velocity.x *= friction;
		this.position.x += this.velocity.x;

    this.velocity.y = constrain(this.velocity.y, -14, 14);

    this.position.x = constrain(this.position.x, 0, game_width - this.width);
    this.position.y = constrain(this.position.y, 0, game_height - this.height);
  }

  display() {
    rect(this.position.x, this.position.y, player_width, player_height);
  }
}
