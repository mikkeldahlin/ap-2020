class Wall {
  constructor(posX, posY, w, h) {
    this.width = w;
    this.height = h
    this.position = createVector(posX, posY);
  }

  run() {
    this.display();
  }

  display() {
    rect(this.position.x, this.position.y, this.width, this.height);
  }
}

function createWall(posX, posY, w, h) {
  tempWall = new Wall(posX, posY, w, h);
  myWalls.push(tempWall);
}
