var playerNub = 2;
var mapY = 870;
var mapX = 1470;
var moveSpeed = 10;
var playerSize = 30;

var p0startX = 0+playerSize*5;
var p0startY = 0+playerSize*5;
var p1startX = mapX-playerSize*7;
var p1startY = mapY-playerSize*7;
var p2startX = mapX-playerSize*7;
var p2startY = 0+playerSize*5;
var p3startX = 0+playerSize*5;
var p3startY = mapY-playerSize*7;

var gridSize = playerSize;
var myPlayers = [];
var grid = [];

function setup() {
  createCanvas(mapX, mapY);
  background(0);
  stroke(0);

  for (i = 0; i < mapX/gridSize; i++) {
    grid[i] = new Array();
    for (j = 0; j < mapY/gridSize; j++) {
      tempGrid = new Grid(i*gridSize, j*gridSize)
      grid[i][j] = tempGrid;
    }
  }
  for (i = 0; i < playerNub; i++ ) {
    tempPLayer = new Player(i);
    myPlayers.push(tempPLayer);
  }
}


function draw() {
  background(0);
  for (i = 0; i < mapX/gridSize; i++) {
    for (j = 0; j < mapY/gridSize; j++) {
    grid[i][j].display();
    }
  }
  for (i = 0; i < playerNub; i++) {
      myPlayers[i].run();
  }
}
