class Player {
  constructor(nub) {
    var posX;
    var posY;
    this.state = nub;
    this.tail = nub + 0.5;

    if (nub == 0) {
      posX = p0startX;
      posY = p0startY;
      this.c = color(255, 0, 0);
      grid[p0startX/gridSize][p0startY/gridSize].state = this.state;
      grid[p0startX/gridSize][p0startY/gridSize-1].state = this.state;
      grid[p0startX/gridSize][p0startY/gridSize+1].state = this.state;
      grid[p0startX/gridSize-1][p0startY/gridSize-1].state = this.state;
      grid[p0startX/gridSize-1][p0startY/gridSize].state = this.state;
      grid[p0startX/gridSize-1][p0startY/gridSize+1].state = this.state;
      grid[p0startX/gridSize+1][p0startY/gridSize-1].state = this.state;
      grid[p0startX/gridSize+1][p0startY/gridSize].state = this.state;
      grid[p0startX/gridSize+1][p0startY/gridSize+1].state = this.state;
      this.up = 87;
      this.right = 68;
      this.down = 83;
      this.left = 65;
    }
    else if (nub == 1) {
      posX = p1startX;
      posY = p1startY;
      this.c = color(0, 0, 255);
      grid[p1startX/gridSize][p1startY/gridSize].state = this.state;
      grid[p1startX/gridSize][p1startY/gridSize-1].state = this.state;
      grid[p1startX/gridSize][p1startY/gridSize+1].state = this.state;
      grid[p1startX/gridSize-1][p1startY/gridSize].state = this.state;
      grid[p1startX/gridSize-1][p1startY/gridSize+1].state = this.state;
      grid[p1startX/gridSize-1][p1startY/gridSize-1].state = this.state;
      grid[p1startX/gridSize+1][p1startY/gridSize].state = this.state;
      grid[p1startX/gridSize+1][p1startY/gridSize-1].state = this.state;
      grid[p1startX/gridSize+1][p1startY/gridSize+1].state = this.state;
      this.up = UP_ARROW;
      this.right = RIGHT_ARROW;
      this.down = DOWN_ARROW;
      this.left = LEFT_ARROW;
    }
    else if (nub == 2) {
      posX = p2startX;
      posY = p2startY;
      this.c = color(0, 255, 0);
      grid[p2startX/gridSize][p2startY/gridSize].state = this.state;
      grid[p2startX/gridSize][p2startY/gridSize-1].state = this.state;
      grid[p2startX/gridSize][p2startY/gridSize+1].state = this.state;
      grid[p2startX/gridSize-1][p2startY/gridSize].state = this.state;
      grid[p2startX/gridSize-1][p2startY/gridSize-1].state = this.state;
      grid[p2startX/gridSize-1][p2startY/gridSize+1].state = this.state;
      grid[p2startX/gridSize+1][p2startY/gridSize].state = this.state;
      grid[p2startX/gridSize+1][p2startY/gridSize-1].state = this.state;
      grid[p2startX/gridSize+1][p2startY/gridSize+1].state = this.state;
      this.up = 104;
      this.right = 102;
      this.down = 101;
      this.left = 100;
    }
    else if (nub == 3) {
      posX = p3startX;
      posY = p3startY;
      this.c = color(255, 255, 0);
      grid[p3startX/gridSize][p3startY/gridSize].state = this.state;
      grid[p3startX/gridSize][p3startY/gridSize-1].state = this.state;
      grid[p3startX/gridSize][p3startY/gridSize+1].state = this.state;
      grid[p3startX/gridSize-1][p3startY/gridSize].state = this.state;
      grid[p3startX/gridSize-1][p3startY/gridSize-1].state = this.state;
      grid[p3startX/gridSize-1][p3startY/gridSize+1].state = this.state;
      grid[p3startX/gridSize+1][p3startY/gridSize].state = this.state;
      grid[p3startX/gridSize+1][p3startY/gridSize-1].state = this.state;
      grid[p3startX/gridSize+1][p3startY/gridSize+1].state = this.state;
      this.up = 73;
      this.right = 76;
      this.down = 75;
      this.left = 74;
    }

    this.active = true;
    this.position = createVector(posX, posY);
    this.moving;
  }

  run() {
    if (this.active) {
      this.input();
      this.move();
      this.display();
    }
  }

  input() {
    if (gridX.includes(this.position.x) && gridY.includes(this.position.y)) {
      var currGridState = grid[this.position.x/gridSize][this.position.y/gridSize].state;

      if (currGridState == this.state) {
        for (i = 0; i < mapX/gridSize; i++) {
          for (j = 0; j < mapY/gridSize; j++) {
            if (grid[i][j].state == this.tail) {
              grid[i][j].state = this.state;
            }
            else if (grid[i][j].state != (0.5 || 1.5 || 2.5 || 3.5 || 5)) {
              var xArray = [];
              var yArray = [];
              xArray.push(i);
              yArray.push(j);

              var colRight = false;
              for (var r = 0; r < mapX/gridSize; r++) {
                if (grid[i+r][j].state == 5) {
                  break
                }
                else if (grid[i+r][j].state == this.state || grid[i+r][j].state == this.tail) {
                  colRight = true;
                  break;
                }
                else {
                  xArray.push(i+r);
                  yArray.push(j);
                }
              }
              if (colRight) {
                var colLeft = false;
                for (var l = 0; l < mapX/gridSize; l++) {
                  if (grid[i-l][j].state == 5) {
                    break
                  }
                  else if (grid[i-l][j].state == this.state || grid[i-l][j].state == this.tail) {
                    colLeft = true;
                    break;
                  }
                  else {
                    xArray.push(i-l);
                    yArray.push(j);
                  }
                }
              }
              if (colLeft) {
                var colUp = false;
                for (var u = 0; u < mapY/gridSize; u++) {
                  if (grid[i][j-u].state == 5) {
                    break
                  }
                  else if (grid[i][j-u].state == this.state || grid[i][j-u].state == this.tail) {
                    colUp = true;
                    break;
                  }
                  else {
                    xArray.push(i);
                    yArray.push(j-u);
                  }
                }
              }
              if (colUp) {
                var colDown = false;
                for (var d = 0; d < mapY/gridSize; d++) {
                  if (grid[i][j+d].state == 5) {
                    break
                  }
                  else if (grid[i][j+d].state == this.state || grid[i][j+d].state == this.tail) {
                    colDown = true;
                    break;
                  }
                  else {
                    xArray.push(i);
                    yArray.push(j+d);
                  }                  }
              }
              if (colDown && colUp && colLeft && colRight) {
                for (var t = 0; t < xArray.length; t++) {
                  var conquredX = xArray[t];
                  var conquredY = yArray[t];
                  grid[conquredX][conquredY].state = this.state;
                }
              }
            }
          }
        }
      }

      else if (currGridState % 1 == 0) {
        grid[this.position.x/gridSize][this.position.y/gridSize].state = this.tail;
      }

      else {
        switch(currGridState) {
          case 0.5:
            removePlayer(0);
            grid[this.position.x/gridSize][this.position.y/gridSize].state = this.tail;
            break;
          case 1.5:
            removePlayer(1);
            grid[this.position.x/gridSize][this.position.y/gridSize].state = this.tail;
          break;
          case 2.5:
            removePlayer(2);
            grid[this.position.x/gridSize][this.position.y/gridSize].state = this.tail;
            break;
          case 3.5:
            removePlayer(3);
            grid[this.position.x/gridSize][this.position.y/gridSize].state = this.tail;
            break;
          default:
        }
      }

      //cannot be here or maybe can be rewritten to fit all? challenge
      if (keyIsDown(this.up) && this.moving !=2) {
        this.moving = 0;
      }
      if (keyIsDown(this.right) && this.moving !=3) {
        this.moving = 1;
      }
      if (keyIsDown(this.down) && this.moving !=0) {
        this.moving = 2;
      }
      if (keyIsDown(this.left) && this.moving !=1) {
        this.moving = 3;
      }
    }
  }

  move() {

    if (this.moving == 0) {
      this.position.y -= moveSpeed;
    }
    if (this.moving == 1) {
      this.position.x += moveSpeed;
    }
    if (this.moving == 2) {
      this.position.y += moveSpeed;
    }
    if (this.moving == 3) {
      this.position.x -= moveSpeed;
    }

    this.position.x = constrain(this.position.x, playerSize, mapX - playerSize*2)
    this.position.y = constrain(this.position.y, playerSize, mapY - playerSize*2)
  }

  display() {
    strokeWeight(5);
    fill(this.c);
    square(this.position.x, this.position.y, playerSize);

  }
}

function removePlayer(n) {
  myPlayers[n].active = false;

  for (i = 0; i < mapX/gridSize; i++) {
    for (j = 0; j < mapY/gridSize; j++) {
      if (grid[i][j].state == myPlayers[n].tail || grid[i][j].state == myPlayers[n].state) {
        grid[i][j].state = 4;
      }
    }
  }
}
