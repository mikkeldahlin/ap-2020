var gridX = [];
var gridY = [];

class Grid {
  constructor(posX, posY) {
    if (!gridX.includes(posX)) {
      gridX.push(posX);
    }
    if (!gridY.includes(posY)) {
      gridY.push(posY);
    }
    this.state = 4;
    if (posX == 0 || posY == 0 || posX == mapX-gridSize || posY == mapY-gridSize) {
      this.state = 5;
    }
    this.position = createVector(posX, posY);
    this.size = gridSize;
  }

  display() {
    strokeWeight(1);
    switch(this.state) {
      case 0:
        fill(255, 0, 0);
        break;
      case 0.5:
        fill(255, 0, 0, 127);
        break;
      case 1:
        fill(0, 0, 255);
        break;
      case 1.5:
        fill(0, 0, 255, 127);
        break;
      case 2:
        fill(0, 255, 0);
        break;
      case 2.5:
        fill(0, 255, 0, 127);
        break;
      case 3:
        fill(255, 255, 0);
        break;
      case 3.5:
        fill(255, 255, 0, 127);
        break;
      case 4:
        fill(0);
        break;
      case 5:
        strokeWeight(0);
        fill(127);
        break;
      default:
    }
    rect(this.position.x, this.position.y, gridSize, gridSize);
  }
}
