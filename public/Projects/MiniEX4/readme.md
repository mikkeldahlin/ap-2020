https://mikkeldahlin.gitlab.io/ap-2020/Projects/MiniEX4/

Pong - when I focus on learning to program, I prefer to have a very defined goal, therefore I set out to recreate the old Pong arcade game. One of the first games ever played on a computer. Another reason for this was, that I wanted to make a bigger program and see if I could keep tabs on what is what in a 200+ line code. Structure and recognizable variable names have been important to me, so I really hope the code makes sort of sense to anyone who reads it. <br></br>
I wanted to make a fully completed program that had various input. I had already worked quite a bit with mouseX and mouseY in earlier MiniEx’s so this time I wanted to work with keycapture. Another reason to choose a simple game like pong. WS and arrow-up/down is used to interact with the paddles. But both a multi-player mode and an impossible singleplayer mode is available if you are just one person playing. <br></br>
Another thing I wanted to learn more about was buttons. In this case I needed them to get sound working in chorme (needs a user input before it can play any) so to het the player "permission" to do use the speakers, i guess i hid it behind the buttons that unfreeze the game when a mode has been selected. In this case the buttons only turn on the sound for the browser, but it might as well also have turned on the webcam without the user ever knowing. This shows how any action we take on the internet might not be what we expect - just because a button says it does something it doesn't necessarily means that it is the all it does. Unless we know the capabilities of something, we cannot hope to know the outcome of an action.  <br></br>
In memory of Kazuhisa Hashimoto, who passed a few days ago, I have included the konami (up up down down left right left right b a) code to unlock another colormode. Hopefully his work will live on forever in games to come.

![ScreenShot](ss.png)
(super secret color mode)

