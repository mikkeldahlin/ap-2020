//field size
//feel free to change these, the code is made to adapt baseded off these two variables
let xwidth = 1200;
let yheight = 600;

//paddle starting pos
let arrowMove = yheight/2-40;
let wsMove = yheight/2-40;

//ball starting pos
let ballXmove = xwidth/2;
let ballYmove = yheight/2;

//ball speed control
let ballSpeedMod = xwidth/200;
let ballSpeedX = -ballSpeedMod;
let ballSpeedY = -ballSpeedMod;

//paddleSpeed control
let paddleSpeed = yheight/100;

//goal counters
let goalCounter1 = 0;
let goalCounter2 = 0;

//startbutton
let started = false;
let buttonpos = [250, 75];

//ball trail
let trailHistory = [];

//vars for number of players and AI's
watching = false;
soloplayer = false;

//up up down down left right left right b a
let konamiCode = [38,38,40,40,37,39,37,39,66,65];
let konamiCounter = 0;
let konamiActivated = false;
let colorOffset = 0;

function setup() {
  createCanvas(xwidth, yheight);
  frameRate(60);

//preload pong sounds
  pongSound1 = loadSound('pong1.mp3');
  pongSound2 = loadSound('pong2.mp3');
  pongSound3 = loadSound('pong3.mp3');
  pongSound4 = loadSound('pong4.mp3');

//create start buttons and link to starting function
  watchbutton = createButton('Watch');
  watchbutton.style('font-size', '40px');
  watchbutton.size(buttonpos[0], buttonpos[1]);
  watchbutton.position(xwidth/2-buttonpos[0]/2, yheight/2-buttonpos[1]/2-80);
  watchbutton.mouseReleased(watch);

  singleplayerbutton = createButton('Singleplayer');
  singleplayerbutton.style('font-size', '40px');
  singleplayerbutton.size(buttonpos[0], buttonpos[1]);
  singleplayerbutton.position(xwidth/2-buttonpos[0]/2, yheight/2-buttonpos[1]/2);
  singleplayerbutton.mouseReleased(singleplayer);

  multiplayerbutton = createButton('Multiplayer');
  multiplayerbutton.style('font-size', '40px');
  multiplayerbutton.size(buttonpos[0], buttonpos[1]);
  multiplayerbutton.position(xwidth/2-buttonpos[0]/2, yheight/2-buttonpos[1]/2+80);
  multiplayerbutton.mouseReleased(multiplayer);
}

function draw() {
  background(0);
  noStroke();

//middle line
  fill(255, 55);
  rectMode(CENTER);
  rect(xwidth/2, yheight/2, xwidth/150, yheight)
  fill(255);

//speedcap
  if (ballSpeedX >= 29){
    ballSpeedX = 29;
  }

//super secret color mode
  if (konamiActivated) {
    colorMode(HSB);
    for(let y = 0; y < height; y += yheight/100) {
      let h = (y / height) * 360;
      fill(abs(h+colorOffset)%360, 100, 100);
    }
    colorOffset -= 8;
  }

//More super secret stuff
  if(konamiActivated && ballSpeedX < 0){
    lastHitColor = ('red');
    textSize(25);
    text('RIP Kazuhisa Hashimoto 1958/10/15 - 2020/02/25 ', xwidth/2, yheight-25);
  }
  else if(konamiActivated && ballSpeedX > 0){
    lastHitColor = ('blue');
    textSize(25);
    text('RIP Kazuhisa Hashimoto 1958/10/15 - 2020/02/25 ', xwidth/2, yheight-25);
  }

//ball
  rectMode(CENTER);
  rect(ballXmove, ballYmove, 20, 20);

  ballXmove += ballSpeedX;
  ballYmove += ballSpeedY;

//paddle 1
  rectMode(CORNER);
  rect(xwidth-50, arrowMove, 30, 80);

  if (keyIsDown(UP_ARROW) && arrowMove >= 10) {
    arrowMove = arrowMove-paddleSpeed;
  }

  if (keyIsDown(DOWN_ARROW) && arrowMove <= yheight-90) {
    arrowMove = arrowMove+paddleSpeed;
  }

//paddle 2

  rect(20, wsMove, 30, 80);

  if (keyIsDown(87) && wsMove >= 10) {
    wsMove = wsMove-paddleSpeed;
  }

  if (keyIsDown(83) && wsMove <= yheight-90) {
    wsMove = wsMove+paddleSpeed;
  }

//goalcounter
  textSize((xwidth+yheight)/35);

  textAlign(LEFT);
  text(goalCounter1, xwidth/2+10, 50);

  textAlign(RIGHT);
  text(goalCounter2, xwidth/2-10, 50);

  textAlign(CENTER);
  text('-', xwidth/2, 50);

//ball trail
    rectMode(CENTER);
   if (started == true) {
     colorMode(RGB, 255);

     let vector = createVector(ballXmove, ballYmove);
     trailHistory.push(vector);

     if (trailHistory.length > 25) {
        trailHistory.splice(0 , 1);
      }

     for (let i = 0; i < trailHistory.length; i++) {
       fill(255, i);
       rect(trailHistory[i].x, trailHistory[i].y, 20, 20);
     }
   }

//side bounce
  if (ballYmove >= yheight-5) {
    ballSpeedY *= -1;
    pongSound3.play();
  }
  if (ballYmove <= 0+5) {
    ballSpeedY *= -1;
    pongSound3.play();
  }

//paddle bounce
  if (ballXmove >= xwidth-60 && ballXmove <= xwidth-30 && ballYmove - (arrowMove+40) <= 45 && ballYmove - (arrowMove+40) >= -45) {
    ballSpeedX *= -1;
    ballSpeedX=ballSpeedX-1;
    pongSound1.play();
  }
  if (ballXmove >= 30 && ballXmove <= 60 && ballYmove - (wsMove+40) <= 45 && ballYmove - (wsMove+40) >= -45) {
    ballSpeedX *= -1;
    ballSpeedX=ballSpeedX+1;
    pongSound2.play();
  }

//goal reset
  if (ballXmove > xwidth+20) {
    ballXmove = xwidth/2;
    ballYmove = yheight/2;
    goalCounter2++;
    ballSpeedX = ballSpeedMod;
    pongSound4.play();
  }

  if (ballXmove < -20) {
    ballXmove = xwidth/2;
    ballYmove = yheight/2;
    goalCounter1++;
    ballSpeedX = -ballSpeedMod;
    pongSound4.play();
  }

// pause before start
  if (started === false) {
    arrowMove = yheight/2-40;
    wsMove = yheight/2-40;
    ballXmove = xwidth/2;
    ballYmove = yheight/2;
  }

//AI controls
  if (watching) {
    arrowMove = ballYmove-40;
    wsMove = ballYmove-40;
  }

  if (soloplayer) {
    wsMove = ballYmove-40;
  }
}

//konami checker
function keyPressed() {
  if (konamiCode[konamiCounter] == keyCode) {
    konamiCounter++;
    if (konamiCounter == konamiCode.length) {
      konamiActivated = true;
      konamiCounter = 0;
    }
  }
  else {
    konamiCounter = konamiCode[0] == keyCode ? 1 : 0;
  }
}

// Start game according to the choosen mode and remove buttons
function watch(){
  started = true;
  watching = true;
  watchbutton.style('display', 'none');
  singleplayerbutton.style('display', 'none');
  multiplayerbutton.style('display', 'none');
}

function singleplayer(){
  started = true;
  watchbutton.style('display', 'none');
  singleplayerbutton.style('display', 'none');
  multiplayerbutton.style('display', 'none');
  soloplayer = true;
}

function multiplayer(){
  started = true;
  watchbutton.style('display', 'none');
  singleplayerbutton.style('display', 'none');
  multiplayerbutton.style('display', 'none');
}
