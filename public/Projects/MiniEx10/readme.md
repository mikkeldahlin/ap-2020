Idea #1

![ScreenShot](flowchart1.png)

Idea #2

![ScreenShot](flowchart2.png)

MiniEx 7.1

![ScreenShot](MiniEx7.1 Flowchart.png)

What are the challenges between simplicity at the level of communication and complexity at the level of algorithmic procedure?

Picking and choosing what highlight  and how to set up an explanation. Code is rarely easy to read and understand. So we are looking for the absolutely essential parts needed to understand what is going in the program. But these parts are usually long and can be hard to shorten to a point where they can fit in a flowchart. It can help a lot not to think about what you want to put in the flowchart, but instead think of writing a pseudocode for your program and then put that in a flowchart instead.


What is the value of the individual and the group flowchart that you have produced?

It is both a way to communicate your program to someone that either does not understand code that well, or someone that is not part of the coding side of a project. But it can also be a great way to communicate, plan and explore with a group. Both before the programming begins and after to see what parts should be programmed first, second and so on.
