let c = 0;
let my = 50;
let mx = 30;
let skinR = 255;
let skinG = 255;
let skinB = 0;

function preload() {
  img = loadImage("skin.png");
}

function setup() {

createCanvas(windowWidth, windowHeight);

}

function draw() {
  background(255)
  image(img, 0, 0, 300, 200);
  translate(windowWidth/2, windowHeight/2);
  strokeWeight(5);

//face
  if (mouseIsPressed && mouseX < 300 && mouseY < 200) {
    c = get(mouseX, mouseY);
  }

  if (mouseIsPressed) {
    skinR = red(c);
    skinG = green(c);
    skinB = blue(c);
  }

  fill(skinR, skinG, skinB);
  circle(0, 0, 200);

  //eyes
  fill(0);
  circle(30,-10,25);
  circle(-30,-10,25);

  //mouth
  if (mouseIsPressed && mouseX > windowWidth/2-40 && mouseX < windowWidth/2+40 && mouseY < windowHeight/2+35 && mouseY > windowHeight/2-50) {
    my = (windowHeight/2-mouseY+25);
    mx = (windowWidth/2-mouseX);
  }

  bezier(-mx, 30, 0, my, 0, my, mx, 30);

}
