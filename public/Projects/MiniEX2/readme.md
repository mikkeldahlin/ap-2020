![ScreenShot](ss.png)

https://mikkeldahlin.gitlab.io/ap-2020/Projects/MiniEX2/

I do not care about the issue of inclusion and diversity with emojis, to me the yellow blobs where fine just the way they were. 
Though after making this program I do think it is kind of fun to be able to edit you own emoji. 
First, I wanted to make much more ways to edit the face, hair, eyes, eyebrows, hats and so on. But time and programming skills where not on my side. 
Though I might continue to work on it in the future. Make the canvas compact and maybe some way to copy paste the emoji that is produced.

Notes for user: 
Skin colour can be chosen from the image in the top left corner. 
The program uses a function that gets the x and y coordinate if the mouse is within the border of the image. 
Then use the red(), blue() and green() functions to get the colour channels form those coordinates.

The mouth can be edited by pressing the mouse close to the mouth and then dragging it around until you have the preferred mouth shape.
The program gets the mouse x and y when pressed near the mouth, then use those coordinates to shape a brazier into the shape of a mouth.

